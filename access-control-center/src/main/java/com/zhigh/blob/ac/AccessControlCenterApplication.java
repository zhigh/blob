package com.zhigh.blob.ac;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 权限控制中心启动类
 *
 * @author zhigh
 * @since 2020/6/26 9:29
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class AccessControlCenterApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(AccessControlCenterApplication.class)
                .properties("spring.config.additional-location=classpath:/more/")
                .run(args);
    }

}
