package com.zhigh.blob.ac.application.api;

import com.zhigh.blob.ac.application.external.feign.HealthCheckFacade;
import com.zhigh.blob.common.base.VResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 健康监测
 *
 * @author zhigh
 * @since 2020/6/26 10:14
 */
@RestController
public class HealthAPI {

    @Autowired
    private HealthCheckFacade healthCheckFacade;

    @GetMapping("/ac/health")
    public VResponse<String> health() {
        List<VResponse<String>> health = healthCheckFacade.health();
        for (VResponse<String> stringVResponse : health) {
            if (!"0000".equals(stringVResponse.getCode()))
                return stringVResponse;
        }
        return VResponse.success("OK");
    }
}
