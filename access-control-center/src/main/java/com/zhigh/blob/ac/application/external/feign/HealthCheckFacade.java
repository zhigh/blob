package com.zhigh.blob.ac.application.external.feign;

import com.zhigh.blob.common.base.VResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 健康监测门面类
 *
 * @author zhigh
 * @since 2020/6/26 10:50
 */
@Component
public class HealthCheckFacade {

    @Resource
    private UserCenterHealthCheck userCenterHealthCheck;

    public List<VResponse<String>> health() {
        List<VResponse<String>> result = new ArrayList<>();
        VResponse<String> health = userCenterHealthCheck.health();
        result.add(health);
        return result;
    }
}

