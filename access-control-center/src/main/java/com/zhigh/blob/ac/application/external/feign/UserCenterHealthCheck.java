package com.zhigh.blob.ac.application.external.feign;

import com.zhigh.blob.ac.application.external.feign.fallback.UserCenterHealthCheckFallback;
import com.zhigh.blob.common.base.VResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 健康监测
 *
 * @author zhigh
 * @since 2020/6/26 10:40
 */
@FeignClient(value = "user-center",
        fallback = UserCenterHealthCheckFallback.class,
        path = "/user"
)
public interface UserCenterHealthCheck {

    @GetMapping("health")
    VResponse<String> health();

}
