package com.zhigh.blob.ac.application.external.feign.fallback;

import com.zhigh.blob.ac.application.external.feign.UserCenterHealthCheck;
import com.zhigh.blob.common.base.VResponse;
import com.zhigh.blob.common.constant.ResponseCode;
import org.springframework.stereotype.Component;

/**
 * fallback
 *
 * @author zhigh
 * @since 2020/6/26 10:41
 */
@Component
public class UserCenterHealthCheckFallback implements UserCenterHealthCheck {
    public UserCenterHealthCheckFallback() {
    }

    @Override
    public VResponse<String> health() {
        return VResponse.fail(ResponseCode.DEFAULT_ERROR_CODE, ResponseCode.DEFAULT_ERROR_MSG);
    }
}
