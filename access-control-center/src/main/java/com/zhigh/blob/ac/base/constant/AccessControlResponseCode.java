package com.zhigh.blob.ac.base.constant;

import com.zhigh.blob.ac.base.exception.AccessControlException;
import com.zhigh.blob.common.constant.ResponseCode;

/**
 * @author zhigh
 * @since 2020/6/22 20:16
 */
public interface AccessControlResponseCode<T extends AccessControlException> extends ResponseCode<T> {



    String INVALID_ACCESS_ERROR_CODE = "UE1000";
    String INVALID_ACCESS_ERROR_MSG = "非法访问";

    String ILLEGAL_QUERY_ERROR_CODE = "UE2010";
    String ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG = "非法查询（查询条件不足）";

    String DATA_NOT_FOUND_ERROR_CODE = "UE2020";
    String DATA_NOT_FOUND_ERROR_MSG = "数据未找到";

}
