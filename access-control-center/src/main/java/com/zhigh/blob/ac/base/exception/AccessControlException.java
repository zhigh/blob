package com.zhigh.blob.ac.base.exception;

import com.zhigh.blob.common.base.BaseException;

/**
 * 用户中心业务异常类
 *
 * @author zhigh
 * @since 2020/6/22 20:15
 */
public class AccessControlException extends BaseException {

    public AccessControlException(String code, String msg) {
        super(code, msg);
    }
}
