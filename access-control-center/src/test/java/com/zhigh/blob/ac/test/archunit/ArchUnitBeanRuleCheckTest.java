package com.zhigh.blob.ac.test.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.zhigh.blob.common.base.PersistenceObject;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

/**
 * @author zhigh
 * @since 2020/6/21 8:15
 */
@AnalyzeClasses(
        importOptions = {DemoLocationProvider.class, ImportOption.DoNotIncludeTests.class})
@ArchIgnore
public class ArchUnitBeanRuleCheckTest {

    @ArchTest
    public static final ArchRule persistence_object_must_extends_PersistenceObject = classes()
            .that().resideInAnyPackage("..po")
            .should().beAssignableTo(PersistenceObject.class).because("PO类应该实现PersistenceObject");
}
