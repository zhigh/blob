package com.zhigh.blob.ac.test.archunit;

import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.zhigh.blob.common.base.AggregateRoot;
import com.zhigh.blob.common.base.Entity;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

/**
 * @Author: zhigh
 * @Date: 2020/6/23 8:41
 */
@AnalyzeClasses(
        importOptions = {DemoLocationProvider.class, ImportOption.DoNotIncludeTests.class})
@ArchIgnore
public class ArchUnitEntityRuleCheckTest {

    @ArchTest
    public static final ArchRule extendRule = classes()
            .that().resideInAPackage("..domain.entity..")
            .should(new EntityDefineRuleCondition()).because("实体对象必须继承 EntityObject或者AggregateRoot, 并且其泛型类型必须是当前实体对象或者其子类.");
}

class EntityDefineRuleCondition extends ArchCondition<JavaClass> {

    EntityDefineRuleCondition(String description, Object... args) {
        super(description, args);
    }

    EntityDefineRuleCondition() {
        super("");
    }

    @Override
    public void check(JavaClass javaClass, ConditionEvents events) {
        Class<?> clz = javaClass.reflect();
        if (clz == AggregateRoot.class || clz == Entity.class)
            return;
        Type genericType = clz.getGenericSuperclass();
        boolean flag = (javaClass.isAssignableTo(Entity.class)) && genericType instanceof ParameterizedType &&
                ((ParameterizedType) genericType).getActualTypeArguments()[0] == clz;
        if (!flag)
            events.add(new SimpleConditionEvent(javaClass, false, clz.getName() + "不符合规范." +
                    ""));
    }

}
