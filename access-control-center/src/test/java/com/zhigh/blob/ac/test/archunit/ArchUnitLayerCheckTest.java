package com.zhigh.blob.ac.test.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

/**
 * @author zhigh
 * @since 2020/6/20 17:24
 */
@AnalyzeClasses(
        importOptions = {DemoLocationProvider.class, ImportOption.DoNotIncludeTests.class}
)
@ArchIgnore
public class ArchUnitLayerCheckTest {


    @ArchTest
    public static final ArchRule layerRule = layeredArchitecture()
            .layer("application").definedBy("com..application..")
            .layer("domain").definedBy("com..domain..")
            .layer("infrastructure").definedBy("com..infrastructure..")
            .layer("common").definedBy("com..common..")
            .whereLayer("application").mayNotBeAccessedByAnyLayer()
            .whereLayer("domain").mayOnlyBeAccessedByLayers("application")
            .whereLayer("infrastructure").mayOnlyBeAccessedByLayers("domain", "application");

    @ArchTest
    @ArchIgnore
    public static final ArchRule subLayerRule4Application = layeredArchitecture()
            .layer("application.api").definedBy("com..application.api..")
            .layer("application.service").definedBy("com..application.service..")
            .layer("application.external").definedBy("com..application.external..")
            .layer("application.repository").definedBy("com..application.repository..")
            .whereLayer("application.api").mayOnlyBeAccessedByLayers()
            .whereLayer("application.service").mayOnlyBeAccessedByLayers("application.api")
            .whereLayer("application.external").mayOnlyBeAccessedByLayers("application.service")
            .whereLayer("application.repository").mayOnlyBeAccessedByLayers("application.service");

    @ArchTest
    @ArchIgnore
    public static final ArchRule subLayerRule4Domain = layeredArchitecture()
            .layer("domain.entity").definedBy("com..domain.entity..")
            .layer("domain.service").definedBy("com..domain.service..")
            .layer("domain.external").definedBy("com..domain.external..")
            .layer("domain.repository").definedBy("com..domain.repository..")
            .whereLayer("domain.service").mayOnlyBeAccessedByLayers("domain.entity")
            .whereLayer("domain.external").mayOnlyBeAccessedByLayers("domain.service")
            .whereLayer("domain.repository").mayOnlyBeAccessedByLayers("domain.service");

}
