package com.zhigh.blob.ac.test.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.importer.Location;

import java.util.regex.Pattern;

/**
 * 项目 ImportOption
 *
 * @author zhigh
 * @since 2020/6/20 17:06
 */
public class DemoLocationProvider implements ImportOption {

    private static final Pattern PATTERN = Pattern.compile(".*/com/zhigh/.*");

    @Override
    public boolean includes(Location location) {
        return location.matches(PATTERN);
    }
}
