package com.zhigh.blob.ac.test.base;

import com.zhigh.blob.ac.AccessControlCenterApplication;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zhigh
 * @since 2020/6/20 20:21
 */
@SpringBootTest(classes = AccessControlCenterApplication.class)
@Disabled
public class SpringBooTest {

    @Autowired
    private StringEncryptor stringEncryptor;


    @Test
    void jasyptTest() {
        String encrypt = stringEncryptor.encrypt("666");
        System.out.println(encrypt);
    }
}
