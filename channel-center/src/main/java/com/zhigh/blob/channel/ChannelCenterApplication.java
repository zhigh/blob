package com.zhigh.blob.channel;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 渠道中心启动类
 *
 * @author zhigh
 * @since 2020/6/27 7:29
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ChannelCenterApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ChannelCenterApplication.class)
                .run(args);
    }
}
