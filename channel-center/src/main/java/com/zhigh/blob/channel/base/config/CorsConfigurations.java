package com.zhigh.blob.channel.base.config;

import org.springframework.cloud.gateway.config.GlobalCorsProperties;
import org.springframework.cloud.gateway.handler.RoutePredicateHandlerMapping;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

import java.util.Map;

/**
 * Cors 跨域配置
 *
 * @author zhigh
 * @since 2020/6/28 9:56
 */
@Configuration
public class CorsConfigurations {

    //@Bean
    public CorsWebFilter corsFilter(GlobalCorsProperties properties, RoutePredicateHandlerMapping routePredicateHandlerMapping) {

        System.out.println(routePredicateHandlerMapping);
        Map<String, CorsConfiguration> corsConfigurations = properties.getCorsConfigurations();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        source.setCorsConfigurations(corsConfigurations);
        return new CorsWebFilter(source);
    }


}
