package com.zhigh.blob.channel.base.config;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.ConfigChangeEvent;
import com.alibaba.nacos.api.config.ConfigChangeItem;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.client.config.listener.impl.AbstractConfigChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.event.RefreshRoutesResultEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

/**
 * 网关配置改动监听器
 *
 * @author zhigh
 * @since 2020/6/27 8:18
 */
@Configuration
public class NacosGatewayListenerConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(NacosGatewayListenerConfiguration.class);

    @Autowired(required = false)
    private NacosConfigManager nacosConfigManager;

    @PostConstruct
    public void init() throws NacosException {
        if (nacosConfigManager == null)
            return;
        ConfigService configService = nacosConfigManager.getConfigService();
        configService.addListener("gateway.yaml", "blob", new AbstractConfigChangeListener() {
            @Override
            public void receiveConfigChange(ConfigChangeEvent event) {
                if (logger.isInfoEnabled()) {
                    for (ConfigChangeItem changeItem : event.getChangeItems()) {
                        logger.info("{} changed, old value is {}, new value is {}.", changeItem.getKey(),
                                changeItem.getOldValue(), changeItem.getNewValue());
                    }
                }
            }
        });
    }

    @Bean
    public ApplicationListener<RefreshRoutesResultEvent> refreshRoutesResult() {
        return new ApplicationListener<RefreshRoutesResultEvent>() {
            @Override
            public void onApplicationEvent(@NotNull RefreshRoutesResultEvent event) {
                if (logger.isInfoEnabled()) {
                    if (event.isSuccess())
                        logger.info("Gateway config refresh successful, timestamp: {}.", event.getTimestamp());
                    else {
                        logger.warn("Gateway config refresh failure.", event.getThrowable());
                    }
                }
            }
        };
    }
}



