package com.zhigh.blob.channel.base.constant;

import com.zhigh.blob.channel.base.exception.ChannelException;
import com.zhigh.blob.common.base.BaseException;

import java.util.function.Supplier;

/**
 * 权益中心异常处理
 *
 * @author zhigh
 * @since 2020/6/22 22:30
 */
public enum ChannelError implements ChanenelResponseCode<ChannelException> {

    /**
     * {@link #INVALID_MOBILE_NO_ERROR_CODE}
     */
    INVALID_MOBILE_NO_ERROR(INVALID_MOBILE_NO_ERROR_CODE, INVALID_MOBILE_NO_ERROR_MSG),
    /**
     * {@link #INVALID_EMAIL_NO_ERROR_CODE}
     */
    INVALID_EMAIL_NO_ERROR(INVALID_EMAIL_NO_ERROR_CODE, INVALID_EMAIL_NO_ERROR_MSG),
    /**
     * {@link #INVALID_ACCESS_ERROR_CODE}
     */
    INVALID_ACCESS_ERROR(INVALID_ACCESS_ERROR_CODE, INVALID_ACCESS_ERROR_MSG),
    /**
     * {@link #ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG}
     */
    ILLEGAL_QUERY_LACK_CONDITION_ERROR(ILLEGAL_QUERY_ERROR_CODE, ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG),
    /**
     * {@link #DATA_NOT_FOUND_ERROR_CODE}
     */
    DATA_NOT_FOUND_ERROR(DATA_NOT_FOUND_ERROR_CODE, DATA_NOT_FOUND_ERROR_MSG);


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * *             基本配置其他系统异常信息类可以参考此类            * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    private String code, msg;
    private Supplier<? extends ChannelException> exceptionSupplier;

    ChannelError(String code, String msg) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = () -> new ChannelException(code, msg);
    }

    ChannelError(String code, String msg, Supplier<? extends ChannelException> exceptionSupplier) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = exceptionSupplier;
    }

    @Override
    public ChannelException throwException() {
        throw exceptionSupplier.get();
    }

    @Override
    public ChannelException exception() {
        return exceptionSupplier.get();
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Supplier<? extends BaseException> getExceptionSupplier() {
        return exceptionSupplier;
    }
}
