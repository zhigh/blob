package com.zhigh.blob.channel.base.exception;

import com.zhigh.blob.common.base.BaseException;

/**
 * 用户中心业务异常类
 *
 * @author zhigh
 * @since 2020/6/22 20:15
 */
public class ChannelException extends BaseException {

    public ChannelException(String code, String msg) {
        super(code, msg);
    }
}
