package com.zhigh.blob.channel.test.base;

import org.junit.jupiter.api.Test;

import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * 基本测试类
 *
 * @author zhigh
 * @since 2020/6/26 9:25
 */
public class BaseTest {

    @Test
    void testNetworkInterface() throws Exception{
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            System.out.println(networkInterface.getName());
        }
    }
}
