package com.zhigh.blob.channel.test.base;

import com.zhigh.blob.channel.ChannelCenterApplication;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zhigh
 * @since 2020/6/20 20:21
 */
@SpringBootTest(classes = ChannelCenterApplication.class)
@Disabled
public class SpringBooTest {

    @Autowired
    private StringEncryptor stringEncryptor;


    @Test
    void jasyptTest() {
        String encrypt = stringEncryptor.encrypt("666");
        System.out.println(encrypt);
    }
}
