package com.zhigh.blob.common;

import com.zhigh.blob.common.util.SpringBeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * common 包全局配置类
 * @Author: zhigh
 * @Date: 2020/6/22 14:06
 */
@Configuration
public class CommonConfiguration {

    @Bean
    public SpringBeanUtils springBeanUtils() {
        return new SpringBeanUtils();
    }
}
