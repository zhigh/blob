package com.zhigh.blob.common.base;

import com.zhigh.blob.common.base.impl.DefaultResponseObject;

/**
 * 聚合根
 *
 * @author zhigh
 * @since 2020/6/20 19:41
 */
@SuppressWarnings("ALL")
public abstract class AggregateRoot<T extends AggregateRoot> extends Entity<T> {

    protected State state = State.NEW;

    public void state(State state) {
        this.state = state;
    }

    public State state() {
        return state;
    }

    public ResponseObject finish() {
        return new DefaultResponseObject().pair("result", "OK");
    }

    public T start() {
        return (T) this;
    }

    public T handle() {
        return (T) this;
    }

    public T success() {
        return (T) this;
    }

    public T fail() {
        return (T) this;
    }

}
