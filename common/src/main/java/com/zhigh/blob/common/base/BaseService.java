package com.zhigh.blob.common.base;

/**
 * @author zhigh
 * @since 2020/6/24 19:46
 */
public interface BaseService {

    default <T extends AggregateRoot<T>> ResponseObject doBusiness(T entity) {
        return Event.start(entity).initialize().prepare().execute().success().fail().finish();
    }
}
