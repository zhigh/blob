package com.zhigh.blob.common.base;

import com.zhigh.blob.common.util.Objects;

import java.io.Serializable;

/**
 * DTO 基类
 *
 * @author zhigh
 * @since 2020/6/22 19:21
 */
public interface DataTransferObject<RqO extends RequestObject, RpO extends ResponseObject> extends Serializable {

    public static <DTO extends DataTransferObject> DTO fromRequest(RequestObject<DTO> requestObject) {
        if (Objects.isNull(requestObject))
            return null;
        return requestObject.toDTO();
    }

    default RpO toRepObj() {
        throw new UnsupportedOperationException("尚未实现 toReqObj 方法");
    }
}
