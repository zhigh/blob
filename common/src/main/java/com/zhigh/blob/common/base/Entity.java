package com.zhigh.blob.common.base;

import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.common.util.SpringBeanUtils;
import com.zhigh.blob.common.util.StringUtils;

import java.io.Serializable;

import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE;

/**
 * 实体对象基类
 * 定义了
 * - 实体对象增删改查的基本方法
 * - 主键的生成
 *
 * @Author: zhigh
 * @Date: 2020/6/20 19:40
 */
@SuppressWarnings("ALL")
public abstract class Entity<T extends Entity> implements Serializable {
    
    private String id;

    public String getId() {
        return id;
    }

    public T setId(String id) {
        this.id = id;
        return (T) this;
    }

    public T checkUnique() {
        return (T) this;
    }

    public T beforeUpdateOrInsert() {
        return (T) this;
    }

    public T updateOrInsert() {
        return beforeUpdateOrInsert();
    }

    public T generateId() {
        if (!StringUtils.isEmptyOrNull(this.id))
            return (T) this;
        this.id = Objects.md5ObjectKeyByUUIDAndDate();
        return (T) this;
    }

    public T beforeSelect() {
        return (T) this;
    }

    public T select() {
        return beforeSelect();
    }

    public T beforeDelete() {
        return (T) this;
    }

    public T delete() {
        return beforeDelete();
    }

    public T initialize() {
        return (T) SpringBeanUtils.autowire(this, AUTOWIRE_BY_TYPE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;
        Entity entity = (Entity) o;
        Objects.requireNonNull(getId(), "Id is null.");
        return getId().equals(entity.getId());
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(getId());
    }
}
