package com.zhigh.blob.common.base;

import com.zhigh.blob.common.base.impl.DefaultEvent;
import com.zhigh.blob.common.config.TransactionConfiguration;
import com.zhigh.blob.common.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基本事件接口
 *
 * @author zhigh
 * @since 2020/6/24 11:27
 */
public interface Event<T extends AggregateRoot<T>> {

    Logger logger = LoggerFactory.getLogger(Event.class);

    State state();

    static <R extends AggregateRoot<R>> Event<R> start(R entity) {
        Objects.requireNonNull(entity, "entity 不能为空.");
        return new DefaultEvent<>(entity, TransactionConfiguration.transactionTemplate);
    }

    Event<T> initialize();

    Event<T> prepare();

    Event<T> execute();

    Event<T> fail();

    Event<T> success();

    ResponseObject finish();

}
