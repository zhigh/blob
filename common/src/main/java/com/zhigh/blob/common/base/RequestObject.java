package com.zhigh.blob.common.base;

import java.io.Serializable;

/**
 * 请求参数类基类
 *
 * @author zhigh
 * @since 2020/6/22 19:24
 */
public interface RequestObject<DTO extends DataTransferObject> extends Serializable {

    default DTO toDTO() {
        throw new UnsupportedOperationException("尚未实现 toDTO 方法");
    }

}
