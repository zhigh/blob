package com.zhigh.blob.common.base;

import com.zhigh.blob.common.util.Objects;

import java.io.Serializable;

/**
 * 基本响应类
 *
 * @author zhigh
 * @since 2020/6/22 19:24
 */
public interface ResponseObject extends Serializable {

    public static <RpO extends ResponseObject> RpO fromDTO(DataTransferObject<?, RpO> dto) {
        if (Objects.isNull(dto))
            return null;
        return dto.toRepObj();
    }


}
