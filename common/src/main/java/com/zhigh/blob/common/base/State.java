package com.zhigh.blob.common.base;

/**
 * 业务状态
 *
 * @author zhigh
 * @since 2020/6/24 12:07
 */
@SuppressWarnings("ALL")
public enum State {
    NEW,
    INITIALIZED,
    PREPARED,
    EXECUTED,
    FAILED,
    SUCCEED,
    FINISHED,
    UNKNOWN;

    public boolean isNew() {
        return this == NEW;
    }

    public boolean isInitialized() {
        return this.ordinal() >= INITIALIZED.ordinal();
    }

    public boolean isExecuted() {
        return this.ordinal() >= EXECUTED.ordinal();
    }

    public boolean isPrepared() {
        return this.ordinal() >= PREPARED.ordinal();
    }

    public boolean isFailed() {
        return this == FAILED;
    }

    public boolean isSuccessful() {
        return this == SUCCEED;
    }

    public boolean isFinished() {
        return this == FINISHED;
    }
}
