package com.zhigh.blob.common.base.impl;

import com.zhigh.blob.common.base.DataTransferObject;
import com.zhigh.blob.common.base.ResponseObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认DTO实现对象
 *
 * @author zhigh
 * @since 2020/6/24 21:01
 */
public class DefaultDataTransferObject extends HashMap<String, Object>
        implements DataTransferObject {

    public DefaultDataTransferObject(Map<? extends String, ?> m) {
        super(m);
    }

    public DefaultDataTransferObject() {
    }

    public DefaultDataTransferObject pair(String name, String value) {
        put(name, value);
        return this;
    }

    public <T> DefaultDataTransferObject fromObject(T t) {
        return this;
    }

    @Override
    public ResponseObject toRepObj() {
        return new DefaultResponseObject(this);
    }
}
