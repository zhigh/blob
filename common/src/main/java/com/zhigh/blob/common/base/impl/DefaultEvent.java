package com.zhigh.blob.common.base.impl;

import com.zhigh.blob.common.base.AggregateRoot;
import com.zhigh.blob.common.base.BaseException;
import com.zhigh.blob.common.base.Event;
import com.zhigh.blob.common.base.ResponseObject;
import com.zhigh.blob.common.base.State;
import com.zhigh.blob.common.constant.BaseError;
import com.zhigh.blob.common.constant.ResponseCode;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.zhigh.blob.common.base.State.*;

/**
 * @author zhigh
 * @since 2020/6/25 10:35
 */
@SuppressWarnings("ALL")
public class DefaultEvent<E extends AggregateRoot<E>> implements Event<E> {

    private TransactionTemplate transactionTemplate;

    private E entity;

    private List<BaseException> throwns = new ArrayList<>();

    public DefaultEvent(E entity, TransactionTemplate transactionTemplate) {
        this.entity = entity;
        try {
            if (this.entity.state().isNew()) {
                this.entity.start().state(NEW);
            }
        } catch (BaseException e) {
            setFail(e);
        } catch (RuntimeException e) {
            unknowExceptionHandle(e);
        }
    }

    @Override
    public synchronized State state() {
        return entity.state();
    }

    @Override
    public Event<E> initialize() {
        if (entity.state().isInitialized())
            return this;
        try {
            entity.start().initialize().state(INITIALIZED);
        } catch (BaseException e) {
            setFail(e);
        } catch (RuntimeException e) {
            unknowExceptionHandle(e);
        }
        return this;
    }

    @Override
    public Event<E> prepare() {
        if (entity.state().isInitialized() && !entity.state().isPrepared())
            entity.state(PREPARED);
        return this;
    }

    @Override
    public Event<E> execute() {
        if (!entity.state().isPrepared() || entity.state().isExecuted())
            return this;
        try {
            entity.handle().state(EXECUTED);
        } catch (BaseException e) {
            setFail(e);
        } catch (RuntimeException e) {
            unknowExceptionHandle(e);
        }
        return this;
    }

    @Override
    public Event<E> fail() {
        if (!entity.state().isFailed())
            return this;
        try {
            entity.fail();
        } catch (BaseException e) {
            setFail(e);
        } catch (RuntimeException e) {
            unknowExceptionHandle(e);
        }
        return this;
    }

    @Override
    public Event<E> success() {
        if (entity.state().isFailed() || !entity.state().isExecuted())
            return this;
        try {
            entity.success().state(SUCCEED);
        } catch (BaseException e) {
            setFail(e);
        } catch (RuntimeException e) {
            unknowExceptionHandle(e);
        }
        return this;
    }

    @Override
    public ResponseObject finish() {
        if (!entity.state().isFailed() && !entity.state().isSuccessful()) {
            entity.state(UNKNOWN);
            throw new BaseException(ResponseCode.FLOW_ERROR_CODE, ResponseCode.FLOW_ERROR_MSG);
        }
        ResponseObject response;
        try {
            response = entity.finish();
        } catch (RuntimeException e) {
            throw e;
        }
        if (entity.state().isFailed()) {
            if (throwns.size() == 1)
                throw throwns.get(0);
            else {
                if (logger.isDebugEnabled()) {
                    for (BaseException thrown : throwns) {
                        logger.debug("error", thrown);
                    }
                }
                throw throwns.get(0);
            }
        }
        entity.state(FINISHED);
        return response;
    }

    private void setFail(BaseException e) {
        throwns.add(e);
        entity.state(FAILED);
    }

    private void unknowExceptionHandle(RuntimeException e) {
        if (throwns.isEmpty())
            throw e;
        // TODO 此处异常处理有待商榷
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
            for (BaseException thrown : throwns) {
                thrown.printStackTrace();
            }
        }
        throw BaseError.DEFAULT_ERROR.exception();
    }
}
