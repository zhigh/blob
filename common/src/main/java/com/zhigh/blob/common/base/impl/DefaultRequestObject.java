package com.zhigh.blob.common.base.impl;

import com.zhigh.blob.common.base.RequestObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认请求类实现
 *
 * @author zhigh
 * @since 2020/6/24 21:05
 */
public class DefaultRequestObject extends HashMap<String,Object>
        implements RequestObject<DefaultDataTransferObject> {

    public DefaultRequestObject() {
    }

    public DefaultRequestObject(Map<? extends String, ?> m) {
        super(m);
    }

    @Override
    public DefaultDataTransferObject toDTO() {
        return new DefaultDataTransferObject(this);
    }

}
