package com.zhigh.blob.common.base.impl;

import com.zhigh.blob.common.base.ResponseObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认响应类实现
 *
 * @author zhigh
 * @since 2020/6/24 21:02
 */
public class DefaultResponseObject extends HashMap<String, Object> implements ResponseObject {

    public DefaultResponseObject() {
    }

    public DefaultResponseObject(Map<? extends String, ?> m) {
        super(m);
    }

    public DefaultResponseObject pair(String name, Object value) {
        put(name, value);
        return this;
    }
}
