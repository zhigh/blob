package com.zhigh.blob.common.config;

import com.zhigh.blob.common.base.BaseException;
import com.zhigh.blob.common.base.VResponse;
import com.zhigh.blob.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

import static com.zhigh.blob.common.constant.ResponseCode.BINDING_ERROR_CODE;
import static com.zhigh.blob.common.constant.ResponseCode.BINDING_ERROR_MSG;

/**
 * 全局异常处理类
 *
 * @Author: zhigh
 * @Date: 2020/6/20 18:17
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@RestControllerAdvice
@Order
public class GlobalExceptionHandler {

    private static final Logger logger= LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public VResponse unknownException(Throwable t ) {
        if (logger.isDebugEnabled())
            t.printStackTrace();
        return VResponse.defaultError();
    }


    @ExceptionHandler(BaseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse businessException(BaseException e) {
        if (logger.isDebugEnabled())
            e.printStackTrace();
        return VResponse.fail(e.getCode(),e.getMsg());
    }

    @ExceptionHandler({BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse<String> validationException(BindException e) {
        if (logger.isDebugEnabled())
            e.printStackTrace();

        return VResponse.fail(BINDING_ERROR_CODE,
                String.format(BINDING_ERROR_MSG, bindExceptionMsgGenerate(e.getFieldErrors())));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse<String> validationException(MethodArgumentNotValidException e) {
        if (logger.isDebugEnabled())
            e.printStackTrace();
        return VResponse.fail(BINDING_ERROR_CODE,
                String.format(BINDING_ERROR_MSG,
                        bindExceptionMsgGenerate(e.getBindingResult().getFieldErrors())));
    }

    private String bindExceptionMsgGenerate(List<FieldError> errors) {
        StringBuilder builder = new StringBuilder();
        builder.append("errors").append('\n');
        for (FieldError fieldError : errors) {
            builder.append(fieldError.getDefaultMessage())
                    .append(" but your value is ")
                    .append(StringUtils.empty(String.valueOf(fieldError.getRejectedValue()))
                            .orElse("empty"))
                    .append(".")
                    .append('\n');
        }
        return builder.toString();
    }
}
