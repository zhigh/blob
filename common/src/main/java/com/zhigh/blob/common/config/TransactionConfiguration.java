package com.zhigh.blob.common.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author zhigh
 * @since 2020/6/25 10:29
 */
@Configuration
@ConditionalOnClass(TransactionAutoConfiguration.class)
@AutoConfigureAfter(value = {TransactionAutoConfiguration.class,
        TransactionAutoConfiguration.TransactionTemplateConfiguration.class})
@ConditionalOnBean(TransactionTemplate.class)
public class TransactionConfiguration {
    public static TransactionTemplate transactionTemplate;

    public TransactionConfiguration(TransactionTemplate transactionTemplate) {
        TransactionConfiguration.transactionTemplate = transactionTemplate;
    }
}
