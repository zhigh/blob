package com.zhigh.blob.common.constant;

import com.zhigh.blob.common.base.BaseException;

import java.util.function.Supplier;

/**
 * @author zhigh
 * @since 2020/6/22 22:09
 */
public enum BaseError implements ResponseCode<BaseException> {
    /**
     * 默认异常
     */
    DEFAULT_ERROR(DEFAULT_ERROR_CODE, DEFAULT_ERROR_MSG),
    /**
     * 绑定异常
     */
    BINDING_ERROR(BINDING_ERROR_CODE, BINDING_ERROR_MSG),
    /**
     * 实体对象中的bean属性未初始化
     */
    NON_INITIALIZE_ENTITY_BEAN_ERROR(NON_INITIALIZE_ENTITY_BEAN_ERROR_CODE, NON_INITIALIZE_ENTITY_BEAN_ERROR_MSG),

    INVALID_MOBILE_NO_ERROR(INVALID_MOBILE_NO_ERROR_CODE, INVALID_MOBILE_NO_ERROR_MSG),

    INVALID_EMAIL_NO_ERROR(INVALID_EMAIL_NO_ERROR_CODE, INVALID_EMAIL_NO_ERROR_MSG);

    private String code, msg;
    private Supplier<? extends BaseException> exceptionSupplier;

    BaseError(String code, String msg) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = () -> new BaseException(code, msg);
    }

    BaseError(String code, String msg, Supplier<? extends BaseException> exceptionSupplier) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = exceptionSupplier;
    }

    @Override
    public BaseException throwException() {
        throw exceptionSupplier.get();
    }

    @Override
    public BaseException exception() {
        return exceptionSupplier.get();
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Supplier<? extends BaseException> getExceptionSupplier() {
        return exceptionSupplier;
    }
}

