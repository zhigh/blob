package com.zhigh.blob.common.constant;

import com.zhigh.blob.common.base.BaseException;

/**
 * 响应码及响应信息
 *
 * @author zhigh
 * @since 2020/6/20 18:31
 */
public interface ResponseCode<T extends BaseException> {

    String FLOW_ERROR_CODE = "UAK000";
    String FLOW_ERROR_MSG = "业务流程执行错误";



    /**
     * 默认错误响应
     */
    String DEFAULT_ERROR_CODE = "UA0000";
    String DEFAULT_ERROR_MSG = "网络异常";

    String BINDING_ERROR_CODE = "UA0002";
    String BINDING_ERROR_MSG = "数据校验失败: \n %s";

    String NON_INITIALIZE_ENTITY_BEAN_ERROR_CODE = "UA0003";
    String NON_INITIALIZE_ENTITY_BEAN_ERROR_MSG = "实体对象中的bean属性未初始化";


    String INVALID_MOBILE_NO_ERROR_CODE = "UE0007";
    String INVALID_MOBILE_NO_ERROR_MSG = "无效手机号";

    String INVALID_EMAIL_NO_ERROR_CODE = "UE0008";
    String INVALID_EMAIL_NO_ERROR_MSG = "无效邮箱号";


    default T throwException() {
        throw new UnsupportedOperationException();
    }

    default T exception() {
        throw new UnsupportedOperationException();
    }

}
