package com.zhigh.blob.common.constant;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 23:14
 */
public interface UserConstant {

    String MOBILE_TYPE = "1";
    String EMAIL_TYPE = "2";

}
