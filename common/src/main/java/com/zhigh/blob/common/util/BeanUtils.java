package com.zhigh.blob.common.util;

import com.zhigh.blob.common.base.BaseException;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * bean 工具类
 *
 * @author zhigh
 * @since 2020/6/24 23:06
 */
public interface BeanUtils {

    Map<Class<?>, PropertyDescriptor[]> cachedPds =
            Collections.synchronizedMap(new WeakHashMap<>());

    static PropertyDescriptor[] getPropertyDescriptors(Class<?> clz) {
        PropertyDescriptor[] result;
        if ((result = cachedPds.get(clz)) == null) {
            try {
                result = Introspector.getBeanInfo(clz).getPropertyDescriptors();
            } catch (IntrospectionException e) {
                throw new BaseException("AP0001", "工具类异常");
            }
            cachedPds.put(clz, result);
        }
        return result;
    }
}
