package com.zhigh.blob.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * 异常处理工具类
 * @Author: zhigh
 * @Date: 2020/6/23 15:40
 */
@SuppressWarnings("ALL")
public interface ExceptionUtils {

    Logger logger = LoggerFactory.getLogger(ExceptionUtils.class);

    static <T extends RuntimeException>  CaughtException<T> catchThrow(Executor executor, Class<T> exceptionClz) {
        Objects.requireNonNull(exceptionClz, "exceptionClz don't be null.");
        try {
            executor.execute();
        }catch (RuntimeException e) {
            if (!exceptionClz.isAssignableFrom(e.getClass()))
                throw e;
            return new CaughtException<>((T) e, exceptionClz);
        }
        return new CaughtException<>(null, exceptionClz);
    }

    static CaughtException<RuntimeException> catchThrow(Executor executor) {
        return catchThrow(executor, RuntimeException.class);
    }

    @FunctionalInterface
    static interface Executor{
        void execute();
    }

    static class CaughtException<T extends RuntimeException> {
        private List<T> throwns = new ArrayList<>();
        private Class<T> clz;

        CaughtException(T thrown, Class<T> clz) {
            Optional.ofNullable(thrown)
                    .ifPresent(throwns::add);
            this.clz = clz;
        }

        public CaughtException<? extends T> thenThrow(Supplier<? extends T> supplier) {
            if(Objects.isEmpty(throwns))
                return this;
            if (logger.isDebugEnabled())
                throwns.stream().forEach(RuntimeException::printStackTrace);
            throw supplier.get();
        }

        public CaughtException<? extends T> then(Executor executor) {
            if (Objects.isEmpty(throwns))
                return this;
            return catchThrow(executor, (Class<? extends T>) null);
        }
    }
}
