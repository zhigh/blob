package com.zhigh.blob.common.util;

import com.zhigh.blob.common.base.BaseException;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map 工具类
 *
 * @author zhigh
 * @since 2020/6/24 22:25
 */
public interface MapUtils {

    /**
     * 将bean中的属性转存入Map中， 浅拷贝
     * tip: 注意确保目标Map可持久化且存在无参构造方法
     *
     * @param bean             源对象
     * @param mapType          目标类型
     * @param ignoreProperties 忽略的属性
     * @param <T>              实际Map类型 (默认HashMap)
     * @return 映射以后的Map
     */
    static <T extends Map<String, Object>> T transformBean(Object bean, Class<T> mapType, String... ignoreProperties) {
        boolean flag = (bean == null
                || Modifier.isAbstract(mapType.getModifiers())
                || !Map.class.isAssignableFrom(mapType));
        if (flag)
            return null;
        T result;
        try {
            Class<?> beanClass = bean.getClass();
            PropertyDescriptor[] beanPropertyDescriptors = BeanUtils.getPropertyDescriptors(beanClass);
            List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);
            result = mapType.newInstance();
            for (PropertyDescriptor bPd : beanPropertyDescriptors) {
                Method readMethod = bPd.getReadMethod();
                if (readMethod != null && bPd.getWriteMethod() != null
                        && (ignoreList == null || !ignoreList.contains(bPd.getName()))) {

                    if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                        readMethod.setAccessible(true);
                    }
                    Object value = readMethod.invoke(bean);
                    result.put(bPd.getName(), value);
                }
            }
        } catch (Throwable e) {
            throw new BaseException("AP0001", "工具类异常");
        }
        return result;
    }

}
