package com.zhigh.blob.common.util;

import com.zhigh.blob.common.base.BaseException;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * Object 工具类整合
 *
 * @Author: zhigh
 * @Date: 2020/6/21 8:48
 */
public interface Objects {

    static boolean isNull(Object obj) {
        return java.util.Objects.isNull(obj);
    }

    static boolean isNonNull(Object obj) {
        return !isNull(obj);
    }

    static boolean isEmpty(Object obj) {
        return ObjectUtils.isEmpty(obj);
    }

    static boolean equal(Object obj1, Object obj2) {
        return ObjectUtils.nullSafeEquals(obj1,obj2);
    }

    static String toStr(Object obj) {
        return String.valueOf(obj);
    }

    static <T> T requireNonNull(T obj, Supplier<? extends RuntimeException> supplier) {
        if (isNull(obj)) {
            throw java.util.Objects.requireNonNull(supplier).get();
        }
        return obj;
    }

    static <T> T nullElseDefaultValue(T obj, T defaultVal) {
        if (isNull(obj)) {
            return defaultVal;
        }
        return obj;
    }

    static <T> T nullElseDefaultValue(T obj, Supplier<T> defaultVal) {
        if (isNull(obj)) {
            System.out.println(defaultVal.hashCode());
            return defaultVal.get();
        }
        return obj;
    }

    static <T> T requireNonNull(T obj, String message) {
        return java.util.Objects.requireNonNull(obj, message);
    }

    static <T> T[] requireNonNull(Supplier<? extends BaseException> supplier, T... objs ) {
        if (isEmpty(objs))
            throw supplier.get();
        for (T t : objs)
            requireNull(t, supplier);
        return objs;
    }

    static boolean allIsNonNull(Object... obj) {
        if (isNull(obj))
            return false;
        for (Object o : obj) {
            if (isNull(o)) return false;
        }
        return true;
    }

    static String md5ObjectKeyByUUIDAndDate() {
        String original = UUID.randomUUID().toString() + LocalDateTime.now().getNano();
        return DigestUtils.md5DigestAsHex(original.getBytes());
    }

    static String md5UUIDSalt() {
        return DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());
    }

    static void requireNull(Object obj, Supplier<? extends RuntimeException> supplier) {
        if (!isNull(obj)) {
            throw java.util.Objects.requireNonNull(supplier).get();
        }
    }
}
