package com.zhigh.blob.common.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.ResolvableType;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Spring bean Utils
 *
 * @Author: zhigh
 * @Date: 2020/6/22 14:16
 */
@SuppressWarnings("ALL")
public class SpringBeanUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    /**
     * 适当调节 如果高并发环境下使用
     * {@link ConcurrentReferenceHashMap}
     * 访问级别较低时使用
     * {@link Collections#synchronizedMap(Map #WeakHashMap)}
     */
    public static final Map<Key, Object> cachedMap =
            new ConcurrentReferenceHashMap(12, ConcurrentReferenceHashMap.ReferenceType.WEAK);

    public static <T> T getBean(String beanName) {
        checkInitialize();
        return (T) applicationContext.getBean(beanName);
    }

    public static <T> T getBean(Class<T> clz) {
        checkInitialize();
        return applicationContext.getBean(clz);
    }

    public static <T> List<T> getBeans(Class<T> clz, Class<?>... genericClz) {
        checkInitialize();
        Object o = cachedMap.get(Key.of(clz, genericClz));
        if (!Objects.isNull(o)) {
            return (List<T>) o;
        }
        List<T> matchedBeans = new ArrayList<>(1);
        Map<String, T> beansOfType = applicationContext.getBeansOfType(clz);
        for (T value : beansOfType.values()) {
            Type[] genericSuperclass = value.getClass().getGenericInterfaces();
            if (genericSuperclass.length != 1 || !(genericSuperclass[0] instanceof ParameterizedType))
                continue;
            Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass[0]).getActualTypeArguments();
            boolean equals = Arrays.equals(actualTypeArguments, genericClz);
            if (equals)
                matchedBeans.add(value);
        }
        if (matchedBeans.isEmpty())
            return null;
        else {
            cachedMap.put(Key.of(clz, genericClz), matchedBeans);
            return matchedBeans;
        }
    }

    public static <T> T getBean(Class<T> clz, Class<?>... genericClz) {
        List<T> beans = getBeans(clz, genericClz);
        if (null == beans || beans.size() != 1)
            throw new NoUniqueBeanDefinitionException(clz, beans.size(), "no unique bean found.");
        return beans.get(0);
    }

    /**
     * 目前只能支持单一的的接口泛型参数
     *
     * @param clz        被查找的类
     * @param genericClz 泛型类型
     * @param <T>        获取的bean的类型
     * @return 查询的bean
     */
    public static <T> T getBean(Class<T> clz, Class<?> genericClz) {
        List<T> beans = getBeans(clz, genericClz);
        if (null == beans || beans.size() != 1)
            throw new NoUniqueBeanDefinitionException(clz, beans.size(), "no unique bean found.");
        return beans.get(0);
    }

    public static <T> T autowire(T obj, int autowireMode) {
        checkInitialize();
        AutowireCapableBeanFactory autowireCapableBeanFactory
                = applicationContext.getAutowireCapableBeanFactory();
        autowireCapableBeanFactory.autowireBeanProperties(obj, autowireMode, false);
        return obj;
    }

    public static <T> T getBean(String beanName, Class<T> clz) {
        return applicationContext.getBean(beanName, clz);
    }

    private static void checkInitialize() {
        Objects.requireNonNull(applicationContext, () -> new UnsupportedOperationException("Spring 应用环境未发现"));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanUtils.applicationContext = applicationContext;
    }


    public static class Key {
        Class<?> type;
        Class<?>[] genericTypes;

        private Key(Class<?> type, Class<?>[] genericTypes) {
            this.type = type;
            this.genericTypes = genericTypes;
        }

        public static Key of(Class<?> type, Class<?>... genericTypes) {
            return new Key(type, genericTypes);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Key)) return false;
            Key key = (Key) o;
            return java.util.Objects.equals(type, key.type) &&
                    Arrays.equals(genericTypes, key.genericTypes);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(type, Arrays.hashCode(genericTypes));
        }
    }
}
