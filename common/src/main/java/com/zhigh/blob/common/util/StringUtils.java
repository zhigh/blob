package com.zhigh.blob.common.util;

import com.google.common.base.Strings;
import com.zhigh.blob.common.constant.BaseError;
import com.zhigh.blob.common.constant.Regexps;

import java.util.Optional;
import java.util.regex.Matcher;

/**
 * 字符串工具类整合
 *
 * @author zhigh
 * @since 2020/6/21 8:33
 */
public interface StringUtils {

    /**
     * 当为空字符串时候返回合适的字符串
     *
     * @param str 字符串
     * @return Optional<String>
     */
    static Optional<String> empty(String str) {
        if (Strings.isNullOrEmpty(str)) {
            return Optional.empty();
        }
        return Optional.of(str);
    }

    static String empty(String str, String defaultValue) {
        if (Strings.isNullOrEmpty(str)) {
            return defaultValue;
        }
        return str;
    }

    static String padStart(String string, int minLength, char padChar) {
        return Strings.padStart(string, minLength, padChar);
    }

    static boolean isEmptyOrNull(String str) {
        return Strings.isNullOrEmpty(str);
    }


    static String getTelWithoutAreaNo(String tel) {
        Matcher matcher = Regexps.MOBILE_NO_PATTERN.matcher(tel);
        if (matcher.matches()) {
            return matcher.group("body");
        } else
            throw BaseError.INVALID_MOBILE_NO_ERROR.exception();
    }

    static void requireIsTel(String tel) {
        if (Regexps.MOBILE_NO_PATTERN.matcher(tel).matches())
            return;
        throw BaseError.INVALID_MOBILE_NO_ERROR.exception();
    }

    static void requireIsEmail(String email) {
        if (Regexps.EMAIL_PATTERN.matcher(email).matches())
            return;
        throw BaseError.INVALID_EMAIL_NO_ERROR.exception();
    }

    static String getUsernameFromEmail(String email) {
        Matcher matcher = Regexps.EMAIL_PATTERN.matcher(email);
        if (matcher.matches())
            return matcher.group("username");
        throw BaseError.INVALID_EMAIL_NO_ERROR.exception();
    }
}
