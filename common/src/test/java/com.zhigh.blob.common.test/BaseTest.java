package com.zhigh.blob.common.test;

import com.zhigh.blob.common.constant.Regexps;
import com.zhigh.blob.common.util.DateUtils;
import com.zhigh.blob.common.util.Objects;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 基本测试
 *
 * @author zhigh
 * @since 2020/6/20 20:21
 */
@Disabled
public class BaseTest {

    @Test
    void md5Test() {
        System.out.println(DigestUtils.md5DigestAsHex("123".getBytes()));
    }

    @Test
    void regexTest() {
        String dateFormat = Regexps.DATE_FORMAT;

        Pattern compile = Pattern.compile(dateFormat);
        Matcher matcher = compile.matcher("2001-12-22");
        if (matcher.matches()) {
            String group = matcher.group(2);
            System.out.println(group);
        }

    }

    @Test
    void regexTest2() {
        boolean mobileNoMatches = Regexps.MOBILE_NO_PATTERN.matcher("101-15895887037").matches();
        Assertions.assertTrue(mobileNoMatches);
        Matcher emailMatcher = Regexps.EMAIL_PATTERN.matcher("zhigh@email.com");
        boolean emailMatches = emailMatcher.matches();
        if (emailMatches) {
            System.out.println(Arrays.asList(
                    emailMatcher.group("head"),
                    emailMatcher.group("tail")
            ));
        }
        Assertions.assertTrue(emailMatches);
    }

    @Test
    void regexTest3() {
        Pattern timestampPattern = Regexps.TIMESTAMP_PATTERN;
        Matcher matcher = timestampPattern.matcher(System.nanoTime() + "");
        if (matcher.matches()) {
            String ms = matcher.group("NS");
            System.out.println(ms);
        }
    }

    @Test
    void dateUtilsTest() {
        Pattern baseDatePattern = Regexps.BASE_DATE_PATTERN;
        Matcher matcher = baseDatePattern.matcher("2001/02/22");
        Set<String> dateFormat = Regexps.BASE_DATE_PATTERNs.keySet();

        if (matcher.matches()) {
            for (String groupName : dateFormat) {
                String group = matcher.group(groupName);
                if (!Objects.isNull(group)) {
                    String pattern = Regexps.DATE_PATTERN_MAP.get(groupName);
                    System.out.println(pattern);
                    break;
                }
            }
        }

    }

    @Test
    void dateUtilsTest2() {
        Pattern baseDatePattern = Regexps.BASE_TIME_PATTERN;
        System.out.println(baseDatePattern.pattern());
        Matcher matcher = baseDatePattern.matcher("061250");
        Set<String> dateFormat = Regexps.BASE_TIME_PATTERNs.keySet();

        if (matcher.matches()) {
            for (String groupName : dateFormat) {
                String group = matcher.group(groupName);
                System.out.println(group);
                if (!Objects.isNull(group)) {
                    String pattern = Regexps.TIME_PATTERN_MAP.get(groupName);
                    System.out.println(pattern);
                    break;
                }
            }
        }
    }

    @Test
    void dateUtilsTest3() {
        LocalDate date = DateUtils.formatLocalDate("20200612 06:23:14");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest4() {
        LocalDateTime date = DateUtils.formatLocalDateTime("2020/06/12 06:23:14");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest5() {
        LocalTime date = DateUtils.formatLocalTime("06:23:14");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest6() {
        LocalTime of = LocalTime.of(13, 0, 0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
        String format = of.format(formatter);
        System.out.println(format);
        LocalTime parse = LocalTime.parse("12:00:00 下午", formatter);
        System.out.println(parse);
    }

    @Test
    void dateUtilsTest7() {
        long age = DateUtils.age("19960214");
        System.out.println(age);
    }

    @Test
    void dateUtilsTest8() {
        long gapOfDays = DateUtils.gapOfDays("19960214", "20200621");
        System.out.println(gapOfDays);
    }

    @Test
    void dateUtilsTest9() {
        long gapOfDays = DateUtils.gapOfYears("19960214", "20200621");
        System.out.println(gapOfDays);
    }

    @Test
    void dateUtilsTest10() {
        Date date = DateUtils.formatDate("20200612 12:00:00");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest11() {
        ZoneId zoneId = ZoneId.systemDefault();
        System.out.println(zoneId.getId());
    }
}
