package com.zhigh.blob.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * 启动类
 *
 * @Author: zhigh
 * @Date: 2020/6/20 11:19
 */
@SpringBootApplication
public class DemoApplicationEntry {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(DemoApplicationEntry.class, args);
    }

    @Configuration
    @MapperScan(basePackages = "com.zhigh.blob.demo.infrastructure.persistence.impl.mybatis")
    public static class MybatisConfiguration {}
}
