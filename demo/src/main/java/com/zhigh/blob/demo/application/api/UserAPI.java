package com.zhigh.blob.demo.application.api;

import com.zhigh.blob.demo.application.api.assemble.UserRequestAssemble;
import com.zhigh.blob.demo.application.api.request.AddUserRequest;
import com.zhigh.blob.demo.application.api.request.TestRequest;
import com.zhigh.blob.demo.application.service.UserService;
import com.zhigh.blob.demo.common.base.VResponse;
import com.zhigh.blob.demo.domain.entity.User;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

/**
 * 用户API
 *
 * @Author: zhigh
 * @Date: 2020/6/20 17:44
 */
@RestController
@RequestMapping("/user")
public class UserAPI {

    @Autowired private UserRequestAssemble userRequestAssemble;
    @Autowired private UserService service;

    @GetMapping("test")
    public VResponse<TestRequest> test(@Valid @ApiParam TestRequest request) {
        return VResponse.success(request);
    }


    @PostMapping("add")
    public VResponse<String> addUser(@RequestBody @Valid @ApiParam AddUserRequest request) {
        User user = userRequestAssemble.userRequest2User(request);
        service.addUser(user);
        return VResponse.success("注册成功.");
    }
}
