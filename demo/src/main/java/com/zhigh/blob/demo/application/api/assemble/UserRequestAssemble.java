package com.zhigh.blob.demo.application.api.assemble;

import com.zhigh.blob.demo.application.api.request.AddUserRequest;
import com.zhigh.blob.demo.domain.entity.User;
import org.mapstruct.Mapper;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 23:59
 */
@Mapper(componentModel = "spring")
public interface UserRequestAssemble {

    User userRequest2User(AddUserRequest request);

}
