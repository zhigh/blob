package com.zhigh.blob.demo.application.api.request;

import com.zhigh.blob.demo.common.constant.Regexps;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 20:06
 */
@ApiModel
public class AddUserRequest {

    @ApiModelProperty("name")
    @NotBlank(message = "姓名不能为空")
    @Length(max = 50)
    private String name;

    @ApiModelProperty("age")
    @NotBlank(message = "年龄不能为空")
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
