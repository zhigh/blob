package com.zhigh.blob.demo.application.api.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 18:41
 */
@ApiModel
public class TestRequest {

    @NotBlank(message = "id 不能为空")
    @Length(min = 5, max = 10)
    @ApiModelProperty(value = "id")
    public String id;

    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = "姓名")
    public String name;
}
