package com.zhigh.blob.demo.application.repository;

import com.zhigh.blob.demo.domain.assemble.UserAssemble;
import com.zhigh.blob.demo.domain.entity.User;
import com.zhigh.blob.demo.infrastructure.persistence.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * P
 * @Author: zhigh
 * @Date: 2020/6/20 23:18
 */
@Repository
public class UserRepository {

    @Autowired @Qualifier("userInfoMybatisMapper")
    private UserInfoMapper userInfoMapper;

    @Autowired @Qualifier("userInfoFileMapper")
    private UserInfoMapper userInfoMapperFile;

    @Autowired private UserAssemble userAssemble;

    public int addUser(User user) {
        return userInfoMapper.insertOne(userAssemble.user2UserInfo(user));
    }
}
