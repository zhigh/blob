package com.zhigh.blob.demo.application.service;

import com.zhigh.blob.demo.application.repository.UserRepository;
import com.zhigh.blob.demo.domain.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * User Service
 * @Author: zhigh
 * @Date: 2020/6/20 23:13
 */
@Service
public class UserService {
    @Autowired private UserRepository userRepository;

    public void addUser(User user) {
        user.setId(UUID.randomUUID().toString());
        userRepository.addUser(user);
    }
}
