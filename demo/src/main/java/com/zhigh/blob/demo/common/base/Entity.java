package com.zhigh.blob.demo.common.base;

import java.io.Serializable;
import java.util.Objects;

/**
 * 实体对象
 *
 * @Author: zhigh
 * @Date: 2020/6/20 19:40
 */
public abstract class Entity implements Serializable {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;
        Entity entity = (Entity) o;
        Objects.requireNonNull(getId(), "Id is null.");
        return getId().equals(entity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
