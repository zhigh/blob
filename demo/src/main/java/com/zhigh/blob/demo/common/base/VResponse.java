package com.zhigh.blob.demo.common.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zhigh.blob.demo.common.constant.ResponseCode;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * 通用返回类
 * @Author: zhigh
 * @Date: 2020/6/20 17:49
 */
@JsonInclude(
        JsonInclude.Include.NON_EMPTY
)
public class VResponse<T> implements Serializable {
    private static final long serialVersionUID = 695333026233L;

    private final static String DEFAULT_SUCCESS_CODE = "0000";
    private final static String DEFAULT_SUCCESS_MSG = "S";

    private final static VResponse EMPTY_RETURN = new VResponse<>();

    public final static VResponse DEFAULT_FAIL_RETURN = fail(null, null);

    private String code;
    private String msg;
    private T data;
    public VResponse() {}

    private VResponse(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @SuppressWarnings("ALL")
    public static <R> VResponse<R> empty() {
        return (VResponse<R>) EMPTY_RETURN;
    }

    @SuppressWarnings("ALL")
    public static <R> VResponse<R> defaultError() {
        return (VResponse<R>) DEFAULT_FAIL_RETURN;
    }

    public static <R> VResponse<R> success(R data) {
        return success(null, null, data);
    }

    public static <R> VResponse<R> success(String code, String msg, R data) {
        return new VResponse<>(
                Optional.ofNullable(code).orElse(DEFAULT_SUCCESS_CODE),
                Optional.ofNullable(msg).orElse(DEFAULT_SUCCESS_MSG),
                data
        );
    }

    public static <R> VResponse<R> fail(String code, String msg, R data) {
        return new VResponse<>(
                Optional.ofNullable(code).orElse(ResponseCode.DEFAULT_ERROR_CODE),
                Optional.ofNullable(msg).orElse(ResponseCode.DEFAULT_ERROR_MSG),
                data
        );
    }

    public static <R> VResponse<R> fail(String code, String msg) {
        return fail(code, msg, null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VResponse<?> vResponse = (VResponse<?>) o;
        return Objects.equals(code, vResponse.code) &&
                Objects.equals(msg, vResponse.msg) &&
                Objects.equals(data, vResponse.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, msg, data);
    }

    @Override
    public String toString() {
        return "VResponse{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
