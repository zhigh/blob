package com.zhigh.blob.demo.common.config;

import com.zhigh.blob.demo.common.base.BaseException;
import com.zhigh.blob.demo.common.base.VResponse;
import com.zhigh.blob.demo.common.util.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

import static com.zhigh.blob.demo.common.constant.ResponseCode.BINDING_ERROR_CODE;
import static com.zhigh.blob.demo.common.constant.ResponseCode.BINDING_ERROR_MSG;

/**
 * 全局异常处理类
 *
 * @Author: zhigh
 * @Date: 2020/6/20 18:17
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public VResponse unknownException( Throwable t ) {
        return VResponse.defaultError();
    }


    @ExceptionHandler(BaseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse businessException(BaseException e) {
        return VResponse.fail(e.getCode(),e.getMsg());
    }

    @ExceptionHandler({BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse<String> validationException(BindException e) {

        return VResponse.fail(BINDING_ERROR_CODE,
                String.format(BINDING_ERROR_MSG, bindExceptionMsgGenerate(e.getFieldErrors())));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public VResponse<String> validationException(MethodArgumentNotValidException e) {
        return VResponse.fail(BINDING_ERROR_CODE,
                String.format(BINDING_ERROR_MSG,
                        bindExceptionMsgGenerate(e.getBindingResult().getFieldErrors())));
    }

    private String bindExceptionMsgGenerate(List<FieldError> errors) {
        StringBuilder builder = new StringBuilder();
        builder.append("errors").append('\n');
        for (FieldError fieldError : errors) {
            builder.append(fieldError.getDefaultMessage())
                    .append(" but your value is ")
                    .append(StringUtils.empty(String.valueOf(fieldError.getRejectedValue()))
                            .orElse("empty"))
                    .append(".")
                    .append('\n');
        }
        return builder.toString();
    }
}
