package com.zhigh.blob.demo.common.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 项目监控
 *
 * @Author: zhigh
 * @Date: 2020/6/22 8:47
 */
@Configuration
public class MonitorConfiguration {

    @Component("demoHealthMonitor")
    public static class DemoHealthMonitor implements HealthIndicator {
        @Override
        public Health health() {
            return Health.up().withDetail("demo", " 简单的健康监测. ").build();
        }
    }

}
