package com.zhigh.blob.demo.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * swagger2 配置
 * @Author: zhigh
 * @Date: 2020/6/20 12:16
 */

@Configuration
@ConditionalOnProperty(value = "swagger.enabled", havingValue = "true")
@EnableSwagger2
public class Swagger2Configuration {

    @Bean
    public Docket allControllerDocker(
            @Value("${swagger.enabled:false}")  boolean enableSwagger) {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("qz-demo")
                .select()
                .apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.zhigh"))
                .paths(PathSelectors.ant("/**"))
                .build()
                .enable(enableSwagger);
    }

    @Bean
    public ApiInfo apiInfo() {
        Contact contact = new Contact("blob","www.zhigh.blob.com","798227552@qq.com");
        return new ApiInfo("Demo 项目文档","简单的demo",
                "v1.0",
                "www.zhigh.blob.com",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>());
    }
}
