package com.zhigh.blob.demo.common.constant;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 常用正则表达式
 *
 * @Author: zhigh
 * @Date: 2020/6/20 20:09
 */
public class Regexps {


    /**
     * 日期相关
     */
    public static final String DATE_FORMAT;
    public static final Map<String, String> BASE_DATE_PATTERNs;
    public static final Map<String, String> DATE_PATTERN_MAP;
    public static final Pattern BASE_DATE_PATTERN;


    static {
        BASE_DATE_PATTERNs = new HashMap<String, String>() {
            {
                put("DA", "[1-9]\\d{3}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])");
                put("DB", "[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])");
                put("DC", "[1-9]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[1-2][0-9]|3[0-1])");
            }
        };
        DATE_PATTERN_MAP = new HashMap<String, String>() {
            {
                put("DA", "yyyyMMdd");
                put("DB", "yyyy-MM-dd");
                put("DC", "yyyy/MM/dd");
            }
        };
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : BASE_DATE_PATTERNs.entrySet()) {
            builder.append("|")
                    .append("(?<")
                    .append(entry.getKey())
                    .append(">")
                    .append(entry.getValue())
                    .append(")");
        }
        builder.deleteCharAt(0);
        DATE_FORMAT = builder.toString();
        BASE_DATE_PATTERN = Pattern.compile(builder.toString());
    }

    public static final String TIME_FORMAT;
    public static final Map<String, String> BASE_TIME_PATTERNs;
    public static final Map<String, String> TIME_PATTERN_MAP;
    public static final Pattern BASE_TIME_PATTERN;

    static {
        BASE_TIME_PATTERNs = new HashMap<String, String>() {
            {
                put("TA", "([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])");
                put("TB", "([01][0-9]|2[0-3])([0-5][0-9])([0-5][0-9])");
//                put("TC", "(0[1-9]|1[0-2])([0-5][0-9])([0-5][0-9]) [(下午)|(上午)]");
//                put("TD", "(0[1-9]|1[0-2]):([0-5][0-9]):([0-5][0-9]) [(下午)|(上午)]");
            }
        };
        TIME_PATTERN_MAP = new HashMap<String, String>() {
            {
                put("TA", "HH:mm:ss");
                put("TB", "HHmmss");
//                put("TC", "HHmmss a");
//                put("TD", "HH:mm:ss a");
            }
        };
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : BASE_TIME_PATTERNs.entrySet()) {
            builder.append("|")
                    .append("(?<")
                    .append(entry.getKey())
                    .append(">")
                    .append(entry.getValue())
                    .append(")");
        }
        builder.deleteCharAt(0);
        TIME_FORMAT = builder.toString();
        BASE_TIME_PATTERN = Pattern.compile(builder.toString());
    }
}
