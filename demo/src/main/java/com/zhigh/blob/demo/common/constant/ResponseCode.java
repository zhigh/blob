package com.zhigh.blob.demo.common.constant;

/**
 * 响应码及响应信息
 *
 * @Author: zhigh
 * @Date: 2020/6/20 18:31
 */
public interface ResponseCode {

    /**
     * 默认错误响应
     */
    String DEFAULT_ERROR_CODE = "UA0000";
    String DEFAULT_ERROR_MSG = "网络异常";

    String BINDING_ERROR_CODE = "UA0002";
    String BINDING_ERROR_MSG = "数据校验失败: \n %s";
}
