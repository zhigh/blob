package com.zhigh.blob.demo.common.util;

import com.zhigh.blob.demo.common.constant.Regexps;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.YEARS;

/**
 * 日期工具类
 *
 * @Author: zhigh
 * @Date: 2020/6/21 9:00
 */
public class DateUtils {

    private static final Map<String, DateTimeFormatter> dateTimeFormatterCaches = new ConcurrentHashMap<>(6);

    /**
     * 判断一个日期的格式,基本而已
     *
     * @param dateStr 日期字符串
     * @return 日期
     */
    public static LocalDate formatLocalDate(String dateStr) {
        String result = dateStr;
        result = dateMatch(result, Regexps.BASE_DATE_PATTERN);

        DateTimeFormatter dateTimeFormatter =
                dateTimeFormatterCaches.computeIfAbsent(result, DateTimeFormatter::ofPattern);
        return LocalDate.parse(dateStr, dateTimeFormatter);
    }

    public static LocalDateTime formatLocalDateTime(String dateStr) {
        String result = dateStr;
        result = dateMatch(result,  Regexps.BASE_DATE_PATTERN);
        result = dateMatch(result, Regexps.BASE_TIME_PATTERN);
        DateTimeFormatter dateTimeFormatter =
                dateTimeFormatterCaches.computeIfAbsent(result, DateTimeFormatter::ofPattern);
        return LocalDateTime.parse(dateStr, dateTimeFormatter);
    }


    public static LocalTime formatLocalTime(String dateStr) {
        String result = dateStr;
        result = dateMatch(result, Regexps.BASE_TIME_PATTERN);
        DateTimeFormatter dateTimeFormatter =
                dateTimeFormatterCaches.computeIfAbsent(result, DateTimeFormatter::ofPattern);
        TemporalAccessor parse = dateTimeFormatter.parse(dateStr);
        return LocalTime.from(parse);
    }

    public static int age(String birthdayStr) {
        return (int) YEARS.between(formatLocalDate(birthdayStr), LocalDate.now());
    }

    public static int gapOfDays(String fromDateStr, String toDateStr) {
        return (int) DAYS.between(formatLocalDate(fromDateStr), formatLocalDate(toDateStr));
    }

   public static int gapOfYears(String fromDateStr, String toDateStr) {
        return (int) YEARS.between(formatLocalDate(fromDateStr), formatLocalDate(toDateStr));
   }

   public static Date formatDate(String dateStr) {
        return Date.from(formatLocalDateTime(dateStr).toInstant(ZoneOffset.ofHours(8)));
   }

    private static String dateMatch(String dateStr, Pattern pattern) {
        int length = dateStr.length();
        String result = dateStr;
        String patternStr;
        int end;
        Map<String, String> patterStrMap = pattern == Regexps.BASE_DATE_PATTERN ?
                Regexps.DATE_PATTERN_MAP : Regexps.TIME_PATTERN_MAP;

        for (Matcher matcher = pattern.matcher(dateStr);
             matcher.find();
             matcher = pattern.matcher(result).region(end, length)) {

            end = matcher.end();
            for (String groupName : patterStrMap.keySet()) {
                String group = matcher.group(groupName);
                if (!Objects.isNull(group)) {
                    patternStr = patterStrMap.get(groupName);
                    result = matcher.replaceFirst(patternStr);
                    break;
                }
            }
        }
        return result;
    }
}

