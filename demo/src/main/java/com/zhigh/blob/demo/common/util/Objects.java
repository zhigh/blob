package com.zhigh.blob.demo.common.util;

import org.springframework.util.ObjectUtils;

/**
 * Object 工具类整合
 *
 * @Author: zhigh
 * @Date: 2020/6/21 8:48
 */
public interface Objects {

    static boolean isNull(Object obj) {
        return java.util.Objects.isNull(obj);
    }

    static boolean isEmpty(Object obj) {
        return ObjectUtils.isEmpty(obj);
    }

    static boolean equal(Object obj1, Object obj2) {
        return ObjectUtils.nullSafeEquals(obj1,obj2);
    }

    static String toStr(Object obj) {
        return String.valueOf(obj);
    }
}
