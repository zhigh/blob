package com.zhigh.blob.demo.common.util;

import com.google.common.base.Strings;

import java.util.Optional;

/**
 * 字符串工具类整合
 *
 * @Author: zhigh
 * @Date: 2020/6/21 8:33
 */
public interface StringUtils {

    /**
     * 当为空字符串时候返回合适的字符串
     *
     * @param str 字符串
     * @return Optional<String>
     */
    static Optional<String> empty(String str) {
        if(Strings.isNullOrEmpty(str)) {
            return Optional.empty();
        }
        return Optional.of(str);
    }

    static String empty(String str, String defaultValue) {
        if(Strings.isNullOrEmpty(str)) {
            return defaultValue;
        }
        return str;
    }

    static String padStart(String string, int minLength, char padChar) {
       return Strings.padStart(string, minLength, padChar);
    }

    static boolean isEmptyOrNull(String str) {
        return Strings.isNullOrEmpty(str);
    }
}
