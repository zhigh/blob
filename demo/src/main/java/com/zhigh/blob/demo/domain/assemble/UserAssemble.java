package com.zhigh.blob.demo.domain.assemble;

import com.zhigh.blob.demo.domain.entity.User;
import com.zhigh.blob.demo.infrastructure.po.UserInfoPo;
import org.mapstruct.Mapper;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 23:49
 */
@Mapper(componentModel = "spring")
public interface UserAssemble {

    UserInfoPo user2UserInfo(User user);

}
