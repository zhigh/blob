package com.zhigh.blob.demo.domain.entity;

import com.zhigh.blob.demo.common.base.AggregateRoot;

import java.util.Objects;

/**
 * 用户
 *
 * @author zhigh
 * @since 2020/6/20 19:45
 */
public class User extends AggregateRoot {
    private String age;
    private String name;
    private String cardId;
    private String idType;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAge(), getName(), getCardId(), getIdType());
    }
}
