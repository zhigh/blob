package com.zhigh.blob.demo.infrastructure.persistence.impl.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhigh.blob.demo.infrastructure.persistence.mapper.UserInfoMapper;
import com.zhigh.blob.demo.infrastructure.po.UserInfoPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 23:27
 */
@Component
public class UserInfoFileMapper implements UserInfoMapper {

    @Autowired private ObjectMapper jsonMapper;

    @Override
    public int insertOne(UserInfoPo userInfoPo) {
        File file = new File("D:/test/data", "userInfo.txt");
        if (!file.exists()) {
            try {
                Files.createDirectories(Paths.get("D:/test", "data"));
                Files.createFile(Paths.get("D:/test/data/userInfo.txt"));
            } catch (Exception e) {
                //
            }
        }
        try (BufferedWriter writer =
                     new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8))) {
            writer.write(jsonMapper.writeValueAsString(userInfoPo));
            writer.newLine();
            writer.flush();
        } catch (Exception e) {
            //
        }
        return 1;
    }
}
