package com.zhigh.blob.demo.infrastructure.persistence.impl.mybatis;

import com.zhigh.blob.demo.infrastructure.persistence.mapper.UserInfoMapper;
import com.zhigh.blob.demo.infrastructure.po.UserInfoPo;
import org.apache.ibatis.annotations.Insert;

/**
 * userInfoMapper
 * @Author: zhigh
 * @Date: 2020/6/21 7:55
 */
public interface UserInfoMybatisMapper extends UserInfoMapper {

    @Insert("insert into user_info(id, name, age) " +
            "values (#{id} ,#{name} ,#{age} )")
   int insertOne(UserInfoPo userInfoPo);
}
