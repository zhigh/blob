package com.zhigh.blob.demo.infrastructure.persistence.mapper;

import com.zhigh.blob.demo.infrastructure.po.UserInfoPo;

/**
 * UserInfo Mapper
 *
 * @Author: zhigh
 * @Date: 2020/6/20 23:27
 */
public interface UserInfoMapper {

    int insertOne(UserInfoPo userInfoPo);

}
