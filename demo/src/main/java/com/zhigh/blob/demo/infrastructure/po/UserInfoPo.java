package com.zhigh.blob.demo.infrastructure.po;

import com.zhigh.blob.demo.common.base.PersistenceObject;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 23:30
 */
public class UserInfoPo extends PersistenceObject {
    private String id;
    private String name;
    private String age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
