package com.zhigh.blob.demo.test.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.springframework.stereotype.Repository;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

/**
 * 命名检测
 *
 * @Author: zhigh
 * @Date: 2020/6/20 16:40
 */
@AnalyzeClasses(
        importOptions = {DemoLocationProvider.class, ImportOption.DoNotIncludeTests.class})
public class ArchUnitClassNameCheckTest {

    /**
     * api类
     */
    @ArchTest
    public static final ArchRule API_CLASS_MUST_END_WITH_API = classes()
            .that().resideInAPackage("com.zhigh..application.api")
            .should().haveSimpleNameEndingWith("API").because("API类应该以API结尾.");
    @ArchTest
    public static final ArchRule END_WITH_API_MUST_IN_api = classes()
            .that().haveSimpleNameEndingWith("API")
            .should().resideInAPackage("com.zhigh..api").because("API类应该放置在api包下.");

    /**
     * 仓储类
     */
    @ArchTest
    public static final ArchRule REPOSITORY_CLASS_MUST_END_WITH_Repo = classes()
            .that().resideInAPackage("com.zhigh..repository")
            .should().haveSimpleNameEndingWith("Repo")
            .orShould().haveSimpleNameEndingWith("Repository")
            .orShould().beAnnotatedWith(Repository.class)
            .because("仓储类应该以Repo结尾并且使用@Repository注解交给Spring容器管理.");
    @ArchTest
    public static final ArchRule END_WITH_REPO_MUST_IN_repo = classes()
            .that().haveSimpleNameEndingWith("Repo")
                .or()
                .haveSimpleNameEndingWith("Repository")
            .should().resideInAPackage("com.zhigh..repository").because("仓储类应该放置在repository包下.");


    /**
     * service
     */
    @ArchTest
    public static final ArchRule SERVICE_CLASS_MUST_END_WHITH_Service = classes()
            .that().resideInAnyPackage("com..service")
            .should().haveSimpleNameEndingWith("Service").because("业务类应该以Service结尾.");
    @ArchTest
    public static final ArchRule END_WHITH_Service_MUST_IN_PACK_SERVICE = classes()
            .that().haveSimpleNameEndingWith("Service")
            .should(). resideInAnyPackage("com..service").because("业务类应该创建于Service包.");
}
