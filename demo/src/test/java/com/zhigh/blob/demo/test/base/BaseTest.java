package com.zhigh.blob.demo.test.base;

import com.zhigh.blob.demo.DemoApplicationEntry;
import com.zhigh.blob.demo.common.constant.Regexps;
import com.zhigh.blob.demo.common.util.DateUtils;
import com.zhigh.blob.demo.common.util.Objects;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: zhigh
 * @Date: 2020/6/20 20:21
 */
@SpringBootTest(classes = DemoApplicationEntry.class)
public class BaseTest {

    @Autowired private StringEncryptor stringEncryptor;

    @Test
    void regexTest() {
        String dateFormat = Regexps.DATE_FORMAT;

        Pattern compile = Pattern.compile(dateFormat);
        Matcher matcher = compile.matcher("2001-12-22");
        if (matcher.matches()) {
            String group = matcher.group(2);
            System.out.println(group);
        }

    }

    @Test
    void fileTest() throws Exception {
        File file = new File("D:/Development Files/workspace-20200620/blob/demo/target/classes/", "data/userInfo.txt");
        System.out.println(file.getAbsolutePath());
    }

    @Test
    void jasyptTest() {
        String encrypt = stringEncryptor.encrypt("666");
        System.out.println(encrypt);
    }

    @Test
    void dateUtilsTest() {
        Pattern baseDatePattern = Regexps.BASE_DATE_PATTERN;
        Matcher matcher = baseDatePattern.matcher("2001/02/22");
        Set<String> dateFormat = Regexps.BASE_DATE_PATTERNs.keySet();

        if (matcher.matches()) {
            for (String groupName : dateFormat) {
                String group = matcher.group(groupName);
                if (!Objects.isNull(group)) {
                    String pattern = Regexps.DATE_PATTERN_MAP.get(groupName);
                    System.out.println(pattern);
                    break;
                }
            }
        }

    }

    @Test
    void dateUtilsTest2() {
        Pattern baseDatePattern = Regexps.BASE_TIME_PATTERN;
        System.out.println(baseDatePattern.pattern());
        Matcher matcher = baseDatePattern.matcher("061250");
        Set<String> dateFormat = Regexps.BASE_TIME_PATTERNs.keySet();

        if (matcher.matches()) {
            for (String groupName : dateFormat) {
                String group = matcher.group(groupName);
                System.out.println(group);
                if (!Objects.isNull(group)) {
                    String pattern = Regexps.TIME_PATTERN_MAP.get(groupName);
                    System.out.println(pattern);
                    break;
                }
            }
        }
    }
    @Test
    void dateUtilsTest3() {
        LocalDate date = DateUtils.formatLocalDate("20200612 06:23:14");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest4() {
        LocalDateTime date = DateUtils.formatLocalDateTime("2020/06/12 06:23:14");
        System.out.println(date);
    }
    @Test
    void dateUtilsTest5() {
        LocalTime date = DateUtils.formatLocalTime("06:23:14");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest6() {
        LocalTime of = LocalTime.of(13, 0, 0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
        String format = of.format(formatter);
        System.out.println(format);
        LocalTime parse = LocalTime.parse("12:00:00 下午", formatter);
        System.out.println(parse);
    }

    @Test
    void dateUtilsTest7() {
        long age = DateUtils.age("19960214");
        System.out.println(age);
    }

    @Test
    void dateUtilsTest8() {
        long gapOfDays = DateUtils.gapOfDays("19960214", "20200621");
        System.out.println(gapOfDays);
    }

    @Test
    void dateUtilsTest9() {
        long gapOfDays = DateUtils.gapOfYears("19960214", "20200621");
        System.out.println(gapOfDays);
    }
    @Test
    void dateUtilsTest10() {
        Date date = DateUtils.formatDate("20200612 12:00:00");
        System.out.println(date);
    }

    @Test
    void dateUtilsTest11() {
        ZoneId zoneId = ZoneId.systemDefault();
        System.out.println(zoneId.getId());
    }
}
