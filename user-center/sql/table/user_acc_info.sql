/**
  用户账户信息
 */
drop table user.user_acc_info;
create table user.user_acc_info
(
    user_seq  varchar(32) primary key comment '用户序列号',
    acc_type  char(1)     not null comment '账号类型 1-手机, 2-邮箱',
    nick_name varchar(40) not null comment '昵称',
    mobile_no varchar(20) unique comment '手机号',
    email_no  varchar(100) unique comment '邮箱号'
);

