/**
  用户登录信息
 */

drop table user.user_login_info;
create table user.user_login_info
(
    user_seq        varchar(32) primary key comment '用户序列号',
    password        varchar(100) not null comment '登录密码',
    salt            varchar(32) not null comment '加密盐值',
    last_err_date   datetime comment '上次登录失败时间',
    last_login_date datetime comment '上次登录时间',
    err_times       int default 0 comment '登录失败次数',
    register_time   timestamp not null comment '注册时间'
);