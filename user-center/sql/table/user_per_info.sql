/**
  用户个人信息表
 */
drop table user.user_per_info;
create table user.user_per_info
(
    user_seq varchar(32) primary key comment '用户序列号',
    name     varchar(40) comment '用户姓名',
    gender   char(1) default '0' comment '性别 1-男 2-女',
    birthday date comment '出生年月',
    country  varchar(50) comment '国家',
    city     varchar(12) comment '所在城市'
);