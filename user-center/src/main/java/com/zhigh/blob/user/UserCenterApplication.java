package com.zhigh.blob.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 用户中心启动类
 *
 * @author zhigh
 * @since 2020/6/22 14:45
 */
@SpringBootApplication
@EnableDiscoveryClient
public class UserCenterApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(UserCenterApplication.class, args);
    }

    @Configuration
    @MapperScan(basePackages = "com.zhigh.blob.user.infrastructure.persistence.impl.mybatis")
    public static class MybatisConfiguration {
    }
}
