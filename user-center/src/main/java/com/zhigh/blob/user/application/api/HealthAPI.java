package com.zhigh.blob.user.application.api;

import com.zhigh.blob.common.base.VResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 健康监测
 *
 * @author zhigh
 * @since 2020/6/26 10:12
 */
@RestController
public class HealthAPI {

    @RequestMapping("/user/health")
    public VResponse<String> health() {
        return VResponse.success("OK");
    }

}
