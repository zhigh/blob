package com.zhigh.blob.user.application.api;

import com.zhigh.blob.common.base.ResponseObject;
import com.zhigh.blob.common.base.VResponse;
import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.user.application.api.assemble.UserAssembler;
import com.zhigh.blob.user.application.api.request.UserLoginRequest;
import com.zhigh.blob.user.application.api.request.UserRegisterRequest;
import com.zhigh.blob.user.application.service.UserService;
import com.zhigh.blob.user.base.constant.UserError;
import com.zhigh.blob.user.domain.entity.User;
import com.zhigh.blob.user.domain.entity.UserLogin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户API
 *
 * @author zhigh
 * @since 2020/6/22 19:13
 */
@RestController
@RequestMapping("/user")
@Api(value = "User Api")
public class UserAPI {

    @Autowired
    private UserAssembler userAssembler;

    @Autowired
    private UserService userService;

    /**
     * 用户注册api
     *
     * @param request {@link UserRegisterRequest}
     * @return response {@link UserService#doBusiness)}
     * @apiNote {@link User}
     */
    @PostMapping("register")
    @ApiOperation(value = "用户注册", notes = "by zhigh")
    public VResponse<ResponseObject> register(@RequestBody @Valid UserRegisterRequest request) {
        return VResponse.success(userService.doBusiness(
                userAssembler.userRegisterRequest2User(request)
        ));
    }

    /**
     * 用户登录
     *
     * @param request {@link UserLoginRequest}
     * @return response {@link UserService#doBusiness)}
     * @apiNote {@link UserLogin}
     */
    @PostMapping("login")
    @ApiOperation(value = "用户登录", notes = "by zhigh")
    public VResponse<ResponseObject> login(@RequestHeader(required = false) @ApiParam(hidden = true) String sessionId,
                                           @CookieValue(required = false) @ApiParam(hidden = true) String token,
                                           @ApiParam @RequestBody @Valid UserLoginRequest request) {
        if (Objects.allIsNonNull(sessionId, token)
                && !Objects.equal(sessionId, token)) {
            UserError.INVALID_ACCESS_ERROR.throwException();
        }
        return VResponse.success(userService.doBusiness(
                userAssembler.userLoginRequest2User(request, sessionId)
        ));
    }
}
