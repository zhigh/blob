package com.zhigh.blob.user.application.api.assemble;

import com.zhigh.blob.user.application.api.request.UserLoginRequest;
import com.zhigh.blob.user.application.api.request.UserRegisterRequest;
import com.zhigh.blob.user.base.config.MapStructConfig;
import com.zhigh.blob.user.domain.entity.User;
import com.zhigh.blob.user.domain.entity.UserLogin;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * RequestDTO <-> Entity <-> ResponseDTO
 *
 * @author zhigh
 * @since 2020/6/22 17:55
 */
@Mapper(config = MapStructConfig.class)
public interface UserAssembler {

    @Mapping(source = "accountType", target = "userAccInfo.accType")
    @Mapping(source = "accountNo", target = "userAccInfo.accountNo")
    @Mapping(source = "password", target = "userLoginInfo.password")
    User userRegisterRequest2User(UserRegisterRequest source);

    @Mapping(source = "source.accType", target = "userAccInfo.accType")
    @Mapping(source = "source.accountNo", target = "userAccInfo.accountNo")
    @Mapping(source = "source.password", target = "md5Plaintext")
    UserLogin userLoginRequest2User(UserLoginRequest source, String sessionId);
}
