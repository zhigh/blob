package com.zhigh.blob.user.application.api.request;

import com.zhigh.blob.common.base.RequestObject;
import com.zhigh.blob.common.base.impl.DefaultDataTransferObject;
import com.zhigh.blob.common.constant.Regexps;
import com.zhigh.blob.common.util.MapUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 用户注册请求参数
 *
 * @author zhigh
 * @since 2020/6/23 11:55
 */
@ApiModel("用户登录请求参数")
public class UserLoginRequest implements RequestObject<DefaultDataTransferObject> {

    @ApiModelProperty("账号")
    @NotBlank(message = "账户不能为空")
    private String accountNo;

    @ApiModelProperty("账户类型")
    @NotBlank(message = "账户类型不能为空")
    private String accType;

    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty("提交时间")
    @NotBlank(message = "提交时间不能为空")
    @Pattern(regexp = Regexps.TIMESTAMP_PATTERN_STR, message = "请传入时间戳")
    private String submitTime;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    @Override
    public DefaultDataTransferObject toDTO() {
        return MapUtils.transformBean(this, DefaultDataTransferObject.class);
    }
}
