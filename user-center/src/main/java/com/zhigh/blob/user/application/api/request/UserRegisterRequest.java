package com.zhigh.blob.user.application.api.request;

import com.zhigh.blob.common.base.RequestObject;
import com.zhigh.blob.common.base.impl.DefaultDataTransferObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

/**
 * 用户注册 Request
 *
 * @author zhigh
 * @since 2020/6/22 19:19
 */
@ApiModel(description = "用户注册请求参数")
public class UserRegisterRequest implements RequestObject<DefaultDataTransferObject> {

    @ApiModelProperty(value = "账号")
    @NotBlank(message = "账号不能为空")
    private String accountNo;

    @ApiModelProperty(value = "账号类型 1-手机号 2-邮箱号")
    @NotBlank(message = "账号类型不能为空")
    @Pattern(regexp = "[12]", message = "无效账号类型")
    private String accountType;

    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不能为空")
    @Length(min = 32, max = 32, message = "无效密码")
    private String password;


    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRegisterRequest)) return false;
        UserRegisterRequest that = (UserRegisterRequest) o;
        return Objects.equals(getAccountNo(), that.getAccountNo()) &&
                Objects.equals(getAccountType(), that.getAccountType()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccountNo(), getAccountType(), getPassword());
    }

}
