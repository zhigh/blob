package com.zhigh.blob.user.application.api.response;

import com.zhigh.blob.common.base.ResponseObject;

/**
 * 用户注册响应
 *
 * @author zhigh
 * @since 2020/6/22 19:17
 */
public class UserRegisterResponse implements ResponseObject {
    private String userSeq;

    public static UserRegisterResponse of(String userSeq) {
        return new UserRegisterResponse(userSeq);
    }

    public UserRegisterResponse(String userSeq) {
        this.userSeq = userSeq;
    }

    public UserRegisterResponse() {
    }

    public String getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(String userSeq) {
        this.userSeq = userSeq;
    }

}
