package com.zhigh.blob.user.base.config;

import org.mapstruct.MapperConfig;
import org.mapstruct.ReportingPolicy;

/**
 * @author zhigh
 * @since 2020/6/25 9:19
 */
@MapperConfig(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        implementationName = "Default<CLASS_NAME>"
)
public interface MapStructConfig {
}
