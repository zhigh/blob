package com.zhigh.blob.user.base.constant;

import com.zhigh.blob.common.base.BaseException;
import com.zhigh.blob.user.base.exception.UserAccountException;

import java.util.function.Supplier;

/**
 * 用户中心异常处理
 *
 * @author zhigh
 * @since 2020/6/22 22:30
 */
public enum UserError implements UserResponseCode<UserAccountException> {
    /**
     * {@link #USER_ACC_INFO_EMPTY_ERROR_CODE}
     */
    USER_ACC_INFO_EMPTY_ERROR(USER_ACC_INFO_EMPTY_ERROR_CODE, USER_ACC_INFO_EMPTY_ERROR_MSG),
    /**
     * {@link #INVALID_ACC_TYPE_CODE}
     */
    INVALID_ACC_TYPE(INVALID_ACC_TYPE_CODE, INVALID_ACC_TYPE_MSG),
    /**
     * {@link #PASSWORD_EMPTY_ERROR_CODE}
     */
    PASSWORD_EMPTY_ERROR(PASSWORD_EMPTY_ERROR_CODE, PASSWORD_EMPTY_ERROR_MSG),
    /**
     * {@link #DUPLICATE_ID_ERROR_CODE}
     */
    DUPLICATE_ID_ERROR(DUPLICATE_ID_ERROR_CODE, DUPLICATE_ID_ERROR_MSG),
    /**
     * {@link #DUPLICATE_ACCOUNT_NO_ERROR_CODE}
     */
    DUPLICATE_ACCOUNT_NO_ERROR(DUPLICATE_ACCOUNT_NO_ERROR_CODE, DUPLICATE_ACCOUNT_NO_ERROR_MSG),
    /**
     * {@link #PWD_UPDATE_NEED_ENCODED_ERROR_CODE}
     */
    PWD_UPDATE_NEED_ENCODED_ERROR(PWD_UPDATE_NEED_ENCODED_ERROR_CODE, PWD_UPDATE_NEED_ENCODED_ERROR_MSG),
    /**
     * {@link #INVALID_MOBILE_NO_ERROR_CODE}
     */
    INVALID_MOBILE_NO_ERROR(INVALID_MOBILE_NO_ERROR_CODE, INVALID_MOBILE_NO_ERROR_MSG),
    /**
     * {@link #INVALID_EMAIL_NO_ERROR_CODE}
     */
    INVALID_EMAIL_NO_ERROR(INVALID_EMAIL_NO_ERROR_CODE, INVALID_EMAIL_NO_ERROR_MSG),
    /**
     * {@link #INVALID_ACCESS_ERROR_CODE}
     */
    INVALID_ACCESS_ERROR(INVALID_ACCESS_ERROR_CODE, INVALID_ACCESS_ERROR_MSG),
    /**
     * {@link #ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG}
     */
    ILLEGAL_QUERY_LACK_CONDITION_ERROR(ILLEGAL_QUERY_ERROR_CODE, ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG),
    /**
     * {@link #DATA_NOT_FOUND_ERROR_CODE}
     */
    DATA_NOT_FOUND_ERROR(DATA_NOT_FOUND_ERROR_CODE, DATA_NOT_FOUND_ERROR_MSG),
    /**
     * {@link #NO_INITIALIZED_ID_ERROR_CODE}
     */
    NO_INITIALIZED_ID_ERROR(NO_INITIALIZED_ID_ERROR_CODE, NO_INITIALIZED_ID_ERROR_MSG),
    /**
     * {@link #ACC_OR_PWD_NOT_RIGHT_ERROR_CODE}
     */
    ACC_OR_PWD_NOT_RIGHT_ERROR(ACC_OR_PWD_NOT_RIGHT_ERROR_CODE, ACC_OR_PWD_NOT_RIGHT_ERROR_MSG),
    /**
     * {@link #ERROR_LOGIN_TIMES_GT_MAX_ERROR_CODE}
     */
    ERROR_LOGIN_TIMES_GT_MAX_ERROR(ERROR_LOGIN_TIMES_GT_MAX_ERROR_CODE, ERROR_LOGIN_TIMES_GT_MAX_ERROR_MSG);


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * *             基本配置其他系统异常信息类可以参考此类            * *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    private String code, msg;
    private Supplier<? extends UserAccountException> exceptionSupplier;

    UserError(String code, String msg) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = () -> new UserAccountException(code, msg);
    }

    UserError(String code, String msg, Supplier<? extends UserAccountException> exceptionSupplier) {
        this.code = code;
        this.msg = msg;
        this.exceptionSupplier = exceptionSupplier;
    }

    @Override
    public UserAccountException throwException() {
        throw exceptionSupplier.get();
    }

    @Override
    public UserAccountException exception() {
        return exceptionSupplier.get();
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Supplier<? extends BaseException> getExceptionSupplier() {
        return exceptionSupplier;
    }
}
