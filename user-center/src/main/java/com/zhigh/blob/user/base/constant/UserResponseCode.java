package com.zhigh.blob.user.base.constant;

import com.zhigh.blob.common.constant.ResponseCode;
import com.zhigh.blob.user.base.exception.UserAccountException;

/**
 * @author zhigh
 * @since 2020/6/22 20:16
 */
public interface UserResponseCode<T extends UserAccountException> extends ResponseCode<T> {

    String USER_ACC_INFO_EMPTY_ERROR_CODE = "UE0001";
    String USER_ACC_INFO_EMPTY_ERROR_MSG = "用户账号信息为空";

    String INVALID_ACC_TYPE_CODE = "UE0002";
    String INVALID_ACC_TYPE_MSG = "无效账户类型";

    String PASSWORD_EMPTY_ERROR_CODE = "UE0003";
    String PASSWORD_EMPTY_ERROR_MSG = "密码为空或无效";

    String DUPLICATE_ID_ERROR_CODE = "UE0004";
    String DUPLICATE_ID_ERROR_MSG = "重复ID";

    String DUPLICATE_ACCOUNT_NO_ERROR_CODE = "UE0005";
    String DUPLICATE_ACCOUNT_NO_ERROR_MSG = "手机号/邮箱已经注册";

    String PWD_UPDATE_NEED_ENCODED_ERROR_CODE = "UE0006";
    String PWD_UPDATE_NEED_ENCODED_ERROR_MSG = "密码更新需要先加密";

    String NO_INITIALIZED_ID_ERROR_CODE = "UE0009";
    String NO_INITIALIZED_ID_ERROR_MSG = "主键检索未初始化主键信息";

    String ACC_OR_PWD_NOT_RIGHT_ERROR_CODE = "UE0010";
    String ACC_OR_PWD_NOT_RIGHT_ERROR_MSG = "账号或密码错误";

    String ERROR_LOGIN_TIMES_GT_MAX_ERROR_CODE = "UE0011";
    String ERROR_LOGIN_TIMES_GT_MAX_ERROR_MSG = "连续账号或密码输入错误次数已达上限，请稍后再试";


    String INVALID_ACCESS_ERROR_CODE = "UE1000";
    String INVALID_ACCESS_ERROR_MSG = "非法访问";

    String ILLEGAL_QUERY_ERROR_CODE = "UE2010";
    String ILLEGAL_QUERY_LACK_CONDITION_ERROR_MSG = "非法查询（查询条件不足）";

    String DATA_NOT_FOUND_ERROR_CODE = "UE2020";
    String DATA_NOT_FOUND_ERROR_MSG = "数据未找到";

}
