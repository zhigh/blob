package com.zhigh.blob.user.domain.assemble;

import com.zhigh.blob.user.base.config.MapStructConfig;
import com.zhigh.blob.user.domain.entity.UserAccInfo;
import com.zhigh.blob.user.domain.entity.UserLoginInfo;
import com.zhigh.blob.user.domain.entity.UserPerInfo;
import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;
import com.zhigh.blob.user.infrastructure.po.UserLoginInfoPo;
import com.zhigh.blob.user.infrastructure.po.UserPerInfoPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * 用户 Entity <-> Po 装配类
 *
 * @author zhigh
 * @since 2020/6/20 23:49
 */
@Mapper(config = MapStructConfig.class)
public interface UserDomainAssembler {

    @Mapping(source = "id", target = "userSeq")
    UserAccInfoPo entity2UserAccInfoPo(UserAccInfo userAccInfo);

    @Mapping(source = "userSeq", target = "id")
    UserAccInfo po2UserAccInfo(UserAccInfoPo userAccInfoPo);

    @Mapping(source = "id", target = "userSeq")
    UserLoginInfoPo entity2UserLoginInfoPo(UserLoginInfo userLoginInfo);

    @Mapping(source = "userSeq", target = "id")
    UserLoginInfo po2UserLoginInfo(UserLoginInfoPo userLoginInfoPo);

    @Mapping(source = "id", target = "userSeq")
    UserPerInfoPo entity2UserPerInfoPo(UserPerInfo userPerInfo);

    @Mapping(source = "userSeq", target = "id")
    UserPerInfo po2UserPerInfo(UserPerInfoPo userPerInfo);
}
