package com.zhigh.blob.user.domain.entity;

import com.zhigh.blob.common.base.AggregateRoot;
import com.zhigh.blob.common.base.Entity;
import com.zhigh.blob.common.base.ResponseObject;
import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.user.application.api.response.UserRegisterResponse;
import com.zhigh.blob.user.domain.service.UserDomainService;

import java.util.stream.Stream;

import static com.zhigh.blob.user.base.constant.UserError.PASSWORD_EMPTY_ERROR;
import static com.zhigh.blob.user.base.constant.UserError.USER_ACC_INFO_EMPTY_ERROR;
import static java.util.Optional.ofNullable;

/**
 * 用户
 *
 * @author zhigh
 * @flow {@link #initialize#handle#fail#success#finish}
 * @since 2020/6/20 19:45
 */
public class User extends AggregateRoot<User> {

    private UserAccInfo userAccInfo;

    private UserLoginInfo userLoginInfo;

    private UserPerInfo userPerInfo;

    private UserDomainService userDomainService;

    /**
     * 用户注册校验
     *
     * @return this
     */
    User registerCheck() {
        ofNullable(this.userAccInfo)
                .orElseThrow(USER_ACC_INFO_EMPTY_ERROR.getExceptionSupplier())
                .checkUserAccountIsAvailable()
                .defaultNickname();
        ofNullable(this.userLoginInfo)
                .orElseThrow(PASSWORD_EMPTY_ERROR.getExceptionSupplier())
                .checkPasswordValid()
                .encoderPassword();
        return this;
    }

    @Override
    public User start() {
        return this;
    }

    /**
     * 初始化
     */
    @Override
    public User initialize() {
        super.initialize();
        if (null == userPerInfo) userPerInfo = new UserPerInfo();
        if (null == userLoginInfo) userLoginInfo = new UserLoginInfo();
        if (null == userAccInfo) userAccInfo = new UserAccInfo();
        this.generateId();
        return this;
    }

    @Override
    public User handle() {
        return registerCheck().checkUnique().registerPrepare().updateOrInsert();
    }

    @Override
    public User fail() {
        return super.fail();
    }

    @Override
    public User success() {
        return super.success();
    }

    @Override
    public ResponseObject finish() {
        return UserRegisterResponse.of(getId());
    }

    /**
     * registerCheck().registerPrepare();
     * 用户注册数据准备
     */
    User registerPrepare() {
        return this;
    }

    /**
     * 校验id唯一性
     *
     * @return this
     */
    public User checkUnique() {
        userDomainService.checkUserUnique(this);
        return this;
    }

    public User updateOrInsert() {
        beforeUpdateOrInsert();
        userDomainService.insertNewUser(this);
        return this;
    }

    @Override
    public User beforeUpdateOrInsert() {
        Stream.of(userAccInfo, userLoginInfo, userPerInfo)
                .forEach(Entity::beforeUpdateOrInsert);
        return this;
    }

    @Override
    public User generateId() {
        super.generateId();
        Stream.of(userAccInfo, userLoginInfo, userPerInfo)
                .filter(Objects::isNonNull)
                .forEach(e -> e.setId(getId()));
        return this;
    }

    public UserAccInfo getUserAccInfo() {
        return userAccInfo;
    }

    public void setUserAccInfo(UserAccInfo userAccInfo) {
        this.userAccInfo = userAccInfo;
    }

    public UserLoginInfo getUserLoginInfo() {
        return userLoginInfo;
    }

    public void setUserLoginInfo(UserLoginInfo userLoginInfo) {
        this.userLoginInfo = userLoginInfo;
    }

    public UserPerInfo getUserPerInfo() {
        return userPerInfo;
    }

    public void setUserPerInfo(UserPerInfo userPerInfo) {
        this.userPerInfo = userPerInfo;
    }

    public void setUserDomainService(UserDomainService userDomainService) {
        this.userDomainService = userDomainService;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), getUserAccInfo(), getUserLoginInfo(), getUserPerInfo());
    }
}
