package com.zhigh.blob.user.domain.entity;

import com.zhigh.blob.common.base.Entity;
import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.common.util.SpringBeanUtils;
import com.zhigh.blob.common.util.StringUtils;
import com.zhigh.blob.user.base.constant.UserError;
import com.zhigh.blob.user.domain.repository.UserDomainRepository;
import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;

import java.util.Optional;

import static com.zhigh.blob.common.constant.UserConstant.EMAIL_TYPE;
import static com.zhigh.blob.common.constant.UserConstant.MOBILE_TYPE;

/**
 * @author zhigh
 * @since 2020/6/22 17:33
 */
public class UserAccInfo extends Entity<UserAccInfo> {

    /**
     * 账号类型 1-手机, 2-邮箱
     */
    private String accType;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 手机号
     */
    private String mobileNo;

    /**
     * 邮箱号
     */
    private String emailNo;

    private UserDomainRepository userDomainRepository;

    UserAccInfo checkUserAccountIsAvailable() {
        switch (accType) {
            case MOBILE_TYPE:
                StringUtils.requireIsTel(mobileNo);
                break;
            case EMAIL_TYPE:
                StringUtils.requireIsEmail(emailNo);
                break;
            default:
                UserError.INVALID_ACC_TYPE.throwException();
        }
        return this;
    }

    UserAccInfo defaultNickname() {
        switch (accType) {
            case MOBILE_TYPE:
                this.nickName = StringUtils.getTelWithoutAreaNo(mobileNo);
                return this;
            case EMAIL_TYPE:
                this.nickName = StringUtils.getUsernameFromEmail(emailNo);
                return this;
            default:
                throw UserError.INVALID_ACC_TYPE.throwException();
        }
    }

    @Override
    public UserAccInfo select() {
        beforeSelect();
        UserAccInfoPo userAccInfoPo = Optional.ofNullable(userDomainRepository.selectUserAccInfo(this))
                .orElseThrow(UserError.DATA_NOT_FOUND_ERROR.getExceptionSupplier());
        accType = userAccInfoPo.getAccType();
        mobileNo = userAccInfoPo.getMobileNo();
        emailNo = userAccInfoPo.getEmailNo();
        nickName = userAccInfoPo.getNickName();
        setId(userAccInfoPo.getUserSeq());
        return this;
    }

    @Override
    public UserAccInfo beforeSelect() {
        if (Objects.isNull(userDomainRepository))
            userDomainRepository = SpringBeanUtils.getBean(UserDomainRepository.class);
        return this;
    }

    @Override
    public UserAccInfo updateOrInsert() {
        return this;
    }

    public void setAccountNo(String accountNo) {
        switch (accType) {
            case MOBILE_TYPE:
                this.mobileNo = accountNo;
                break;
            case EMAIL_TYPE:
                this.emailNo = accountNo;
                break;
            default:
                UserError.INVALID_ACC_TYPE.throwException();
        }
    }

    public String getAccountNo() {
        switch (accType) {
            case MOBILE_TYPE:
                return this.mobileNo;
            case EMAIL_TYPE:
                return this.emailNo;
            default:
                throw UserError.INVALID_ACC_TYPE.throwException();
        }
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailNo() {
        return emailNo;
    }

    public void setEmailNo(String emailNo) {
        this.emailNo = emailNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAccInfo)) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), getAccType(), getNickName(), getMobileNo(), getEmailNo());
    }
}
