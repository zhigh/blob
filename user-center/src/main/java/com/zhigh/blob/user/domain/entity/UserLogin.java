package com.zhigh.blob.user.domain.entity;

import com.zhigh.blob.common.base.AggregateRoot;
import com.zhigh.blob.common.util.ExceptionUtils;
import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.user.base.constant.UserError;

/**
 * 用户登录
 *
 * @author zhigh
 * @flow {@link #initialize#handle#fail#success#finish}
 * @since 2020/6/23 13:09
 */
public class UserLogin extends AggregateRoot<UserLogin> {
    /**
     * 提交时间
     */
    private String submitTime;
    /**
     * {@link UserLoginInfo}
     */
    private UserLoginInfo userLoginInfo;
    /**
     * {@link UserAccInfo}
     */
    private UserAccInfo userAccInfo;
    /**
     * md5编码的明文密码
     */
    private String md5Plaintext;
    /**
     * sessionId
     */
    private String sessionId;


    @Override
    public UserLogin initialize() {
        userLoginInfo = new UserLoginInfo();
        return this;
    }

    @Override
    public UserLogin select() {
        ExceptionUtils.catchThrow(() -> {
            userAccInfo.select();
            userLoginInfo.setId(userAccInfo.getId()).select();
        }).thenThrow(UserError.ACC_OR_PWD_NOT_RIGHT_ERROR.getExceptionSupplier());
        setId(userAccInfo.getId());
        return this;
    }

    UserLogin validatePassword() {
        userLoginInfo.checkAllowableLogin();
        UserLoginInfo temp = new UserLoginInfo();
        temp.setPassword(md5Plaintext);
        temp.setSalt(userLoginInfo.getSalt());
        String password = temp.checkPasswordValid().encoderPassword().getPassword();
        boolean equal = Objects.equal(password, userLoginInfo.getPassword());
        if (!equal) {
            throw UserError.ACC_OR_PWD_NOT_RIGHT_ERROR.exception();
        }
        return this;
    }

    @Override
    public UserLogin start() {
        return this;
    }

    @Override
    public UserLogin handle() {
        select().validatePassword();
        return this;
    }

    @Override
    public UserLogin fail() {
        userLoginInfo.updateLoginError();
        return this;
    }

    @Override
    public UserLogin success() {
        userLoginInfo.updateLastLoginTime();
        return this;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public UserLoginInfo getUserLoginInfo() {
        return userLoginInfo;
    }

    public void setUserLoginInfo(UserLoginInfo userLoginInfo) {
        this.userLoginInfo = userLoginInfo;
    }

    public UserAccInfo getUserAccInfo() {
        return userAccInfo;
    }

    public void setUserAccInfo(UserAccInfo userAccInfo) {
        this.userAccInfo = userAccInfo;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMd5Plaintext() {
        return md5Plaintext;
    }

    public void setMd5Plaintext(String md5Plaintext) {
        this.md5Plaintext = md5Plaintext;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLogin)) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), getSubmitTime(),
                getUserLoginInfo(), getUserAccInfo(),
                getMd5Plaintext(), getSessionId());
    }
}
