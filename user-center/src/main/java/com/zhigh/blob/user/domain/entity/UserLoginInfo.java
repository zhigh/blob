package com.zhigh.blob.user.domain.entity;

import com.zhigh.blob.common.base.Entity;
import com.zhigh.blob.common.constant.BaseError;
import com.zhigh.blob.common.constant.Regexps;
import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.common.util.SpringBeanUtils;
import com.zhigh.blob.user.base.constant.UserError;
import com.zhigh.blob.user.domain.repository.UserDomainRepository;
import com.zhigh.blob.user.infrastructure.po.UserLoginInfoPo;
import org.springframework.util.DigestUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;

/**
 * entity - 用户登录信息
 *
 * @author zhigh
 * @since 2020/6/22 17:42
 */
public class UserLoginInfo extends Entity<UserLoginInfo> {

    /**
     * 最大登录错误次数
     */
    private static final int MAX_ERROR_LOGIN_TIMES = 3;

    /**
     * 限制登录时长
     */
    private static final Duration REFRESH_LOGIN_STATUS_DURATION = Duration.ofSeconds(30L);

    /**
     * 登录密码
     */
    private String password;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 上次登录失败时间
     */
    private Date lastErrDate;

    /**
     * 上次登录时间
     */
    private Date lastLoginDate;

    /**
     * 登录失败次数
     */
    private Integer errTimes;

    /**
     * 密码是否加密
     */
    private boolean isEncoded = false;

    /**
     * 是否允许登录
     */
    private boolean isAllowableLogin = true;

    private UserDomainRepository userDomainRepository;

    UserLoginInfo checkPasswordValid() {
        if (!isEncoded) {
            Matcher matcher = Regexps.MD5_PATTERN
                    .matcher(Objects.requireNonNull(password,
                            UserError.PASSWORD_EMPTY_ERROR.getExceptionSupplier()));
            if (!matcher.matches())
                throw UserError.PASSWORD_EMPTY_ERROR.exception();
            salt = Optional.ofNullable(salt).orElse(Objects.md5UUIDSalt());
        }
        return this;
    }

    UserLoginInfo checkAllowableLogin() {
        if (lastErrDate != null) {
            int duration = Duration.between(lastErrDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                    LocalDateTime.now()).compareTo(REFRESH_LOGIN_STATUS_DURATION);
            if (duration > 0) {
                errTimes = 0;
            } else if (errTimes != null && errTimes >= MAX_ERROR_LOGIN_TIMES)
                isAllowableLogin = false;
        }
        if (!isAllowableLogin)
            throw UserError.ERROR_LOGIN_TIMES_GT_MAX_ERROR.exception();
        return this;
    }

    UserLoginInfo encoderPassword() {
        password = DigestUtils.md5DigestAsHex(
                (password + salt).getBytes());
        this.isEncoded = true;
        return this;
    }

    @Override
    public UserLoginInfo select() {
        UserLoginInfoPo userLoginInfoPo = Optional.ofNullable(
                beforeSelect().userDomainRepository
                        .selectByPrimaryKey(this)
        ).orElseThrow(UserError.DATA_NOT_FOUND_ERROR.getExceptionSupplier());
        setPassword(userLoginInfoPo.getPassword());
        setSalt(userLoginInfoPo.getSalt());
        setErrTimes(userLoginInfoPo.getErrTimes());
        setLastLoginDate(userLoginInfoPo.getLastLoginDate());
        setLastErrDate(userLoginInfoPo.getLastErrDate());
        return this;
    }

    @Override
    public UserLoginInfo beforeSelect() {
        Optional.ofNullable(getId())
                .orElseThrow(UserError.NO_INITIALIZED_ID_ERROR.getExceptionSupplier());
        if (Objects.isNull(userDomainRepository))
            userDomainRepository = SpringBeanUtils.getBean(UserDomainRepository.class);
        return this;
    }

    @Override
    public UserLoginInfo beforeUpdateOrInsert() {
        if (!isEncoded)
            UserError.PWD_UPDATE_NEED_ENCODED_ERROR.throwException();
        return this;
    }

    UserLoginInfo updateLastLoginTime() {
        Optional.ofNullable(userDomainRepository)
                .orElseThrow(BaseError.NON_INITIALIZE_ENTITY_BEAN_ERROR.getExceptionSupplier())
                .updateLastLoginTime(this);
        return this;
    }

    UserLoginInfo updateLoginError() {
        if (errTimes == null) errTimes = 0;
        if (isAllowableLogin) {
            errTimes++;
            Optional.ofNullable(userDomainRepository)
                    .orElseThrow(BaseError.NON_INITIALIZE_ENTITY_BEAN_ERROR.getExceptionSupplier())
                    .updateLoginError(this);
        }
        return this;
    }


    @Override
    public UserLoginInfo updateOrInsert() {
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastErrDate() {
        return lastErrDate;
    }

    public void setLastErrDate(Date lastErrDate) {
        this.lastErrDate = lastErrDate;
    }

    public Date get() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getErrTimes() {
        return errTimes;
    }

    public void setErrTimes(Integer errTimes) {
        this.errTimes = errTimes;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
