package com.zhigh.blob.user.domain.entity;

import com.zhigh.blob.common.base.Entity;

import java.util.Date;

/**
 * 用户个人信息
 *
 * @author zhigh
 * @since 2020/6/22 17:43
 */
public class UserPerInfo extends Entity<UserPerInfo> {

    /**
     * 用户姓名
     */
    private String name;

    /**
     * 性别 1-男 2-女
     */
    private String gender;

    /**
     * 出生年月
     */
    private Date birthday;

    /**
     * 国家
     */
    private String country;

    /**
     * 所在城市
     */
    private String city;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
