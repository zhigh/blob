package com.zhigh.blob.user.domain.repository;

import com.zhigh.blob.user.base.constant.UserError;
import com.zhigh.blob.user.domain.assemble.UserDomainAssembler;
import com.zhigh.blob.user.domain.entity.UserAccInfo;
import com.zhigh.blob.user.domain.entity.UserLoginInfo;
import com.zhigh.blob.user.domain.entity.UserPerInfo;
import com.zhigh.blob.user.infrastructure.persistence.mapper.UserAccInfoMapper;
import com.zhigh.blob.user.infrastructure.persistence.mapper.UserLoginInfoMapper;
import com.zhigh.blob.user.infrastructure.persistence.mapper.UserPerInfoMapper;
import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;
import com.zhigh.blob.user.infrastructure.po.UserLoginInfoPo;
import com.zhigh.blob.user.infrastructure.po.UserPerInfoPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import static com.zhigh.blob.common.util.Objects.isNull;

/**
 * 领域层仓储类，（ 同时也算实体业务类 ）
 *
 * @author zhigh
 * @since 2020/6/22 21:00
 */
@Repository
public class UserDomainRepository {

    @Resource(name = "userAccInfoMybatisMapper")
    private UserAccInfoMapper userAccInfoMapper;

    @Resource(name = "userLoginInfoMybatisMapper")
    private UserLoginInfoMapper userLoginInfoMapper;

    @Resource(name = "userPerInfoMybatisMapper")
    private UserPerInfoMapper userPerInfoMapper;

    @Autowired
    private UserDomainAssembler userDomainAssembler;

    public Object selectByUserSeq(String userSeq) {
        return userAccInfoMapper.selectByPrimaryKey(userSeq);
    }

    public int insertOrUpdateUserAccInfo(UserAccInfo userAccInfo) {
        return userAccInfoMapper.
                insertOrUpdateUserAccInfo(userDomainAssembler.entity2UserAccInfoPo(userAccInfo));
    }

    public int insertOrUpdateUserLoginInfo(UserLoginInfo UserLoginInfo) {
        UserLoginInfoPo userLoginInfoPo = userDomainAssembler.entity2UserLoginInfoPo(UserLoginInfo);
        if (userLoginInfoMapper.updateByPrimaryKey(userLoginInfoPo) == 0)
            return userLoginInfoMapper.insert(userLoginInfoPo);
        return 0;
    }

    public int insertOrUpdateUserPerInfo(UserPerInfo userPerInfo) {
        UserPerInfoPo userPerInfoPo = userDomainAssembler.entity2UserPerInfoPo(userPerInfo);
        if (userPerInfoMapper.updateByPrimaryKey(userPerInfoPo) == 0)
            return userPerInfoMapper.insert(userPerInfoPo);
        return 0;
    }

    public UserAccInfoPo checkAccountUnique(UserAccInfo userAccInfo) {
        return userAccInfoMapper.selectByAccountNo(userDomainAssembler.entity2UserAccInfoPo(userAccInfo));
    }

    public UserAccInfoPo selectUserAccInfo(UserAccInfo userAccInfo) {
        UserAccInfoPo userAccInfoPo = userDomainAssembler.entity2UserAccInfoPo(userAccInfo);
        if (userAccInfoPo.getUserSeq() != null)
            return userAccInfoMapper.selectByPrimaryKey(userAccInfoPo.getUserSeq());
        else if (!isNull(userAccInfo.getAccType()) &&
                (!isNull(userAccInfo.getMobileNo()) || !isNull(userAccInfo.getEmailNo()))) {
            return userAccInfoMapper.selectByAccountNo(userAccInfoPo);
        }
        throw UserError.ILLEGAL_QUERY_LACK_CONDITION_ERROR.exception();
    }

    public UserLoginInfoPo selectByPrimaryKey(UserLoginInfo userLoginInfo) {
        return isNull(userLoginInfo.getId()) ? null :
                userLoginInfoMapper.selectByPrimaryKey(userLoginInfo.getId());
    }

    public int updateLastLoginTime(UserLoginInfo userLoginInfo) {
        return isNull(userLoginInfo.getId()) ? 0 :
                userLoginInfoMapper.updateLastLoginTime(
                        userDomainAssembler.entity2UserLoginInfoPo(userLoginInfo));
    }

    public int updateLoginError(UserLoginInfo userLoginInfo) {
        return isNull(userLoginInfo.getId()) ? 0 :
                userLoginInfoMapper.updateLoginError(
                        userDomainAssembler.entity2UserLoginInfoPo(userLoginInfo));
    }
}
