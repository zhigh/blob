package com.zhigh.blob.user.domain.service;

import com.zhigh.blob.common.util.Objects;
import com.zhigh.blob.user.base.constant.UserError;
import com.zhigh.blob.user.domain.entity.User;
import com.zhigh.blob.user.domain.repository.UserDomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 领域层数据仓储类
 * user
 *
 * @author zhigh
 * @since 2020/6/22 20:59
 */
@Service
public class UserDomainService {

    @Autowired
    private UserDomainRepository userDomainRepository;


    public void checkUserUnique(User user) {
        Objects.requireNull(userDomainRepository.selectByUserSeq(user.getId()),
                UserError.DUPLICATE_ID_ERROR.getExceptionSupplier());
        Objects.requireNull(userDomainRepository.checkAccountUnique(user.getUserAccInfo()),
                UserError.DUPLICATE_ACCOUNT_NO_ERROR.getExceptionSupplier());
    }

    @Transactional
    public void insertNewUser(User user) {
        userDomainRepository.insertOrUpdateUserAccInfo(user.getUserAccInfo());
        userDomainRepository.insertOrUpdateUserLoginInfo(user.getUserLoginInfo());
        userDomainRepository.insertOrUpdateUserPerInfo(user.getUserPerInfo());
    }
}
