package com.zhigh.blob.user.infrastructure.persistence.impl.mybatis;

import com.zhigh.blob.user.infrastructure.persistence.mapper.UserAccInfoMapper;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 17:21
 */
public interface UserAccInfoMybatisMapper extends UserAccInfoMapper {
}
