package com.zhigh.blob.user.infrastructure.persistence.impl.mybatis;

import com.zhigh.blob.user.infrastructure.persistence.mapper.UserLoginInfoMapper;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 17:17
 */
public interface UserLoginInfoMybatisMapper extends UserLoginInfoMapper {
}
