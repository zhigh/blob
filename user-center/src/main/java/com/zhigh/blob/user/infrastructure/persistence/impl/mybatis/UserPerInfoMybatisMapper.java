package com.zhigh.blob.user.infrastructure.persistence.impl.mybatis;

import com.zhigh.blob.user.infrastructure.persistence.mapper.UserPerInfoMapper;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 17:22
 */
public interface UserPerInfoMybatisMapper extends UserPerInfoMapper {
}
