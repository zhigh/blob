package com.zhigh.blob.user.infrastructure.persistence.mapper;

import com.zhigh.blob.user.infrastructure.persistence.mapper.reader.UserAccInfoReader;
import com.zhigh.blob.user.infrastructure.persistence.mapper.writer.UserAccInfoWriter;
import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;

/**
 * @author zhigh
 * @since 2020/6/22 19:31
 */
public interface UserAccInfoMapper extends UserAccInfoReader, UserAccInfoWriter {


    default int insertOrUpdateUserAccInfo(UserAccInfoPo po) {
        if (updateByPrimaryKey(po) == 0)
            return insert(po);
        return 1;
    }

}