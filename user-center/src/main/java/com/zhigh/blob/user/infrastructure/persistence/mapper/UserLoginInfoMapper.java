package com.zhigh.blob.user.infrastructure.persistence.mapper;

import com.zhigh.blob.user.infrastructure.po.UserLoginInfoPo;

/**
 * @author zhigh
 * @since 2020/6/22 19:34
 */
public interface UserLoginInfoMapper {
    int deleteByPrimaryKey(String userSeq);

    int insert(UserLoginInfoPo record);

    int insertSelective(UserLoginInfoPo record);

    UserLoginInfoPo selectByPrimaryKey(String userSeq);

    int updateByPrimaryKeySelective(UserLoginInfoPo record);

    int updateByPrimaryKey(UserLoginInfoPo record);

    int updateLastLoginTime(UserLoginInfoPo record);

    int updateLoginError(UserLoginInfoPo recode);
}