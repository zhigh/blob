package com.zhigh.blob.user.infrastructure.persistence.mapper;

import com.zhigh.blob.user.infrastructure.po.UserPerInfoPo;

/**
* 
* @Author: zhigh
* @Date: 2020/6/22 17:21
*/
public interface UserPerInfoMapper {
    int deleteByPrimaryKey(String userSeq);

    int insert(UserPerInfoPo record);

    int insertSelective(UserPerInfoPo record);

    UserPerInfoPo selectByPrimaryKey(String userSeq);

    int updateByPrimaryKeySelective(UserPerInfoPo record);

    int updateByPrimaryKey(UserPerInfoPo record);
}