package com.zhigh.blob.user.infrastructure.persistence.mapper.reader;

import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;

/**
 * @author zhigh
 * @since 2020/6/25 11:39
 */
public interface UserAccInfoReader {
    UserAccInfoPo selectByPrimaryKey(String userSeq);

    UserAccInfoPo selectByAccountNo(UserAccInfoPo userAccInfoPo);

    int countByAccountNo(UserAccInfoPo record);
}
