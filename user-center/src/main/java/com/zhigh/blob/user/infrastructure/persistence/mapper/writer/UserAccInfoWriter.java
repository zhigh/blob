package com.zhigh.blob.user.infrastructure.persistence.mapper.writer;

import com.zhigh.blob.user.infrastructure.po.UserAccInfoPo;

/**
 * @author zhigh
 * @since 2020/6/25 11:38
 */
public interface UserAccInfoWriter {
    int deleteByPrimaryKey(String userSeq);

    int insert(UserAccInfoPo record);

    int insertSelective(UserAccInfoPo record);

    int updateByPrimaryKeySelective(UserAccInfoPo record);

    int updateByPrimaryKey(UserAccInfoPo record);
}
