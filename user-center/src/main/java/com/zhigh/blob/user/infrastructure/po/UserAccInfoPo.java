package com.zhigh.blob.user.infrastructure.po;

import com.zhigh.blob.common.base.PersistenceObject;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 19:31
 */
public class UserAccInfoPo extends PersistenceObject {
    /**
     * 用户序列号
     */
    private String userSeq;

    /**
     * 账号类型 1-手机, 2-邮箱
     */
    private String accType;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 手机号
     */
    private String mobileNo;

    /**
     * 邮箱号
     */
    private String emailNo;

    public String getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(String userSeq) {
        this.userSeq = userSeq;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailNo() {
        return emailNo;
    }

    public void setEmailNo(String emailNo) {
        this.emailNo = emailNo;
    }
}