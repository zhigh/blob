package com.zhigh.blob.user.infrastructure.po;

import com.zhigh.blob.common.base.PersistenceObject;

import java.util.Date;

/**
 * @Author: zhigh
 * @Date: 2020/6/22 19:34
 */
public class UserLoginInfoPo extends PersistenceObject {
    /**
     * 用户序列号
     */
    private String userSeq;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 上次登录失败时间
     */
    private Date lastErrDate;

    /**
     * 上次登录时间
     */
    private Date lastLoginDate;

    /**
     * 登录失败次数
     */
    private Integer errTimes;

    /**
     * 注册时间
     */
    private Date registerTime;

    public String getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(String userSeq) {
        this.userSeq = userSeq;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getLastErrDate() {
        return lastErrDate;
    }

    public void setLastErrDate(Date lastErrDate) {
        this.lastErrDate = lastErrDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getErrTimes() {
        return errTimes;
    }

    public void setErrTimes(Integer errTimes) {
        this.errTimes = errTimes;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }
}