package com.zhigh.blob.user.infrastructure.po;

import com.zhigh.blob.common.base.PersistenceObject;
import java.util.Date;

/**
* 
* @Author: zhigh
* @Date: 2020/6/22 17:21
*/
public class UserPerInfoPo extends PersistenceObject {
    /**
    * 用户序列号
    */
    private String userSeq;

    /**
    * 用户姓名
    */
    private String name;

    /**
    * 性别 1-男 2-女
    */
    private String gender;

    /**
    * 出生年月
    */
    private Date birthday;

    /**
    * 国家
    */
    private String country;

    /**
    * 所在城市
    */
    private String city;

    public String getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(String userSeq) {
        this.userSeq = userSeq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}