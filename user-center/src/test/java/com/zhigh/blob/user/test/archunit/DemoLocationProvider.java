package com.zhigh.blob.user.test.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.importer.Location;

import java.util.regex.Pattern;

/**
 * 项目 ImportOption
 * @Author: zhigh
 * @Date: 2020/6/20 17:06
 */
public class DemoLocationProvider implements ImportOption {

    private static final Pattern PATTERN = Pattern.compile(".*/com/zhigh/.*");

    @Override
    public boolean includes(Location location) {
        return location.matches(PATTERN);
    }
}
