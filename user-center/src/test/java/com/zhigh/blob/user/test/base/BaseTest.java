package com.zhigh.blob.user.test.base;

import com.zhigh.blob.common.base.DataTransferObject;
import com.zhigh.blob.common.base.impl.DefaultDataTransferObject;
import com.zhigh.blob.common.util.BeanUtils;
import com.zhigh.blob.common.util.MapUtils;
import com.zhigh.blob.common.util.StringUtils;
import com.zhigh.blob.user.domain.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * 基本测试
 *
 * @author zhigh
 * @since 2020/6/23 8:55
 */
public class BaseTest {

    @Test
    void testReflection() {
        Class<User> userClass = User.class;
        Type genericSuperclass = userClass.getGenericSuperclass();
        Assertions.assertTrue(genericSuperclass instanceof ParameterizedType);
    }

    @Test
    void testMapUtils() {
        User user = new User();
        DefaultDataTransferObject defaultDataTransferObject = MapUtils.transformBean(user, DefaultDataTransferObject.class);
        System.out.println(defaultDataTransferObject);
    }

    @Test
    void testMapUtils2() {
        int modifiers = DataTransferObject.class.getModifiers();
        System.out.println(Modifier.isAbstract(modifiers));
    }

    @Test
    void testBeanUtils() {
        PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(User.class);
        System.out.println(Arrays.toString(propertyDescriptors));
    }

    @Test
    void testObjectUtils() {
    }

    @Test
    void testStringUtils() {
        String usernameFromEmail = StringUtils.getUsernameFromEmail("798227552@qq.com.cn");
        System.out.println(usernameFromEmail);
    }
}
