package com.zhigh.blob.user.test.base;

import com.zhigh.blob.user.UserCenterApplication;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zhigh
 * @since 2020/6/20 20:21
 */
@SpringBootTest(classes = UserCenterApplication.class)
@Disabled
public class SpringBooTest {

    @Autowired
    private StringEncryptor stringEncryptor;


    @Test
    void jasyptTest() {
        String encrypt = stringEncryptor.encrypt("666");
        System.out.println(encrypt);
    }
//    @Test
//    void test() {
//        long start = System.currentTimeMillis();
//        User user = new User();
//        for (int i = 0; i < 1000000; i++) {
//            Event.start(user);
//        }
//        System.out.println((System.currentTimeMillis() - start));
//    }
//
//    @Test
//    void test2() {
//        long start = System.currentTimeMillis();
//        User user = new User();
//        IntStream.range(0, 1000000).parallel().forEach(i -> Event.start(user));
//        System.out.println(System.currentTimeMillis() - start);
//    }

}
